import 'dart:async';

import 'package:flutter/material.dart';

class TypingProvider extends ChangeNotifier{
  Set<String> _isTyping = Set();

  ///
  /// Typing countdown will last for 5s
  ///
  Timer _typingCountdown;

  TypingProvider(){
    _typingCountdown = Timer.periodic(Duration(seconds: 5), (it){
      _isTyping.clear();
      notifyListeners();
    });
  }

  void someoneTyping(String userIdUrl){
    _isTyping.add(userIdUrl);
    notifyListeners();
  }

  void remove(String userIdUrl){
    _isTyping.remove(userIdUrl);
    notifyListeners();
  }

  bool isTyping(String userIdUrl) => _isTyping.contains(userIdUrl);

  @override
  void dispose() {
    _typingCountdown.cancel();
    super.dispose();
  }
}