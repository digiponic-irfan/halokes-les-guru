import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:halokes_les_guru/network/response/general/MapelResponse.dart';

class BuatPertemuanArgument {
  final int pertemuanKe;
  final int jumlahPertemuan;
  final List<BuatPertemuanParams> pertemuanParams;
  final BuatJadwalParams jadwalParams;

  BuatPertemuanArgument({@required this.pertemuanKe, @required this.jumlahPertemuan,
    @required this.pertemuanParams, @required this.jadwalParams});

  factory BuatPertemuanArgument.copyWith({BuatPertemuanArgument lastArgument, int pertemuanKe, int jumlahPertemuan,
    List<BuatPertemuanParams> pertemuanParams, BuatJadwalParams jadwalParams}){
    return BuatPertemuanArgument(
      pertemuanKe: pertemuanKe ?? lastArgument.pertemuanKe,
      jumlahPertemuan: jumlahPertemuan ?? lastArgument.jumlahPertemuan,
      pertemuanParams: pertemuanParams ?? lastArgument.pertemuanParams,
      jadwalParams: jadwalParams ?? lastArgument.jadwalParams,
    );
  }
}

class BuatPertemuanParams {
  String materi;
  String tanggal;
  String jamMulai;
  String jamSelesai;

  BuatPertemuanParams({this.materi, this.tanggal, this.jamMulai, this.jamSelesai});

  Map<String, dynamic> asMap() => {
    "materi" : materi,
    "tanggal" : tanggal,
    "jam_mulai" : jamMulai,
    "jam_selesai" : jamSelesai,
  };
}

class BuatJadwalParams {
  final MapelData mapel;
  final String kelas;
  final String harga;

  BuatJadwalParams({@required this.mapel, @required this.kelas, @required this.harga});
}