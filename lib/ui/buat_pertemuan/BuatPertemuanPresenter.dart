import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/extension/Kelas.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';
import 'package:halokes_les_guru/ui/buat_pertemuan/BuatPertemuanArgument.dart';
import 'package:halokes_les_guru/ui/buat_pertemuan/BuatPertemuanDelegate.dart';
import 'package:intl/intl.dart';

class BuatPertemuanPresenter extends BasePresenter {
  static const REQUEST_BUAT_JADWAL = 0;

  final BuatPertemuanDelegate _delegate;

  BuatPertemuanPresenter(BaseState state, this._delegate) : super(state);

  void validate(String materi, DateTime selectedDate,
      int jamMulai, int menitMulai, int jamAkhir, int menitAkhir) {

    if(materi == ""){
      state.alert(title: "Error", message: "Materi belum diisi", positiveTitle: "Tutup");
      return;
    }

    if(selectedDate == null){
      state.alert(title: "Error", message: "Tanggal belum dipilih", positiveTitle: "Tutup");
      return;
    }

    var start = (jamMulai * 60) + menitMulai;
    var end = (jamAkhir * 60) + menitAkhir;

    if(start > end){
      state.alert(title: "Error", message: "Jam mulai tidak bisa lebih besar daripada jam berakhir", positiveTitle: "Tutup");
      return;
    }

    if(start == end){
      state.alert(title: "Error", message: "Jam mulai tidak bisa sama dengan jam berakhir", positiveTitle: "Tutup");
      return;
    }

    if((end - start) < 30){
      state.alert(title: "Error", message: "Sesi minimal berlangsung selama 30 menit", positiveTitle: "Tutup");
      return;
    }

    _delegate.onValid(BuatPertemuanParams(
      materi: materi,
      tanggal: DateFormat("yyyy-MM-dd").format(selectedDate),
      jamMulai: "$jamMulai:$menitMulai:00",
      jamSelesai: "$jamAkhir:$menitAkhir:00",
    ));
  }

  void executeBuatJadwal(BuatPertemuanArgument argument) async {

    var user = await AppPreference.getUser();

    var params = {
      "id_guru" : user.idGuru,
      "id_mapel" : argument.jadwalParams.mapel.idMapelUrl,
      "kelas" : Kelas.angka(argument.jadwalParams.kelas),
      "jumlah" : argument.jumlahPertemuan,
      "harga" : argument.jadwalParams.harga,
    };

    var listDetail = List<Map<String, dynamic>>();
    argument.pertemuanParams.forEach((it) => listDetail.add(it.asMap()));
    params["detail"] = listDetail;

    generalRepo.executeBuatJadwal(REQUEST_BUAT_JADWAL, params, _delegate.onSuccessBuatJadwal);
  }
}