import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseResponse.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/ui/buat_pertemuan/BuatPertemuanArgument.dart';
import 'package:halokes_les_guru/ui/buat_pertemuan/BuatPertemuanDelegate.dart';
import 'package:halokes_les_guru/ui/buat_pertemuan/BuatPertemuanPresenter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:intl/intl.dart';
import 'package:numberpicker/numberpicker.dart';

class BuatPertemuan extends StatefulWidget {
  final BuatPertemuanArgument argument;

  BuatPertemuan({@required this.argument});

  @override
  _BuatPertemuanState createState() => _BuatPertemuanState();
}

class _BuatPertemuanState extends BaseState<BuatPertemuan> with BuatPertemuanDelegate{
  BuatPertemuanPresenter _presenter;

  FocusNode _materiFocus = FocusNode();

  TextEditingController _dateController = TextEditingController();

  String _materi = "";
  DateTime _selectedDate;
  int _startHour = 9;
  int _startMinutes = 0;
  int _endHour = 9;
  int _endMinutes = 0;

  @override
  void initState() {
    super.initState();
    _presenter = BuatPertemuanPresenter(this, this);
  }

  @override
  void onValid(BuatPertemuanParams item) {
    if(widget.argument.pertemuanKe != widget.argument.jumlahPertemuan){
      navigateTo(MyApp.ROUTE_BUAT_PERTEMUAN, arguments: BuatPertemuanArgument.copyWith(
        lastArgument: widget.argument,
        pertemuanKe: widget.argument.pertemuanKe + 1,
        pertemuanParams: widget.argument.pertemuanParams..add(item),
      ));
    }else {
      _presenter.executeBuatJadwal(BuatPertemuanArgument.copyWith(
        lastArgument: widget.argument,
        pertemuanParams: widget.argument.pertemuanParams..add(item),
      ));
    }
  }

  @override
  void onSuccessBuatJadwal(BaseResponse response) {
    navigateTo(MyApp.ROUTE_SUCCESS_BUAT_PERTEMUAN);
  }

  @override
  void onDialogResult(tag, result) {
    if(tag == BaseState.DEFAULT_DATE_PICKER_TAG){
      _selectedDate = result;
      _dateController.text = DateFormat("dd/MM/yyyy").format(_selectedDate);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StyledText("Pertemuan ke ${widget.argument.pertemuanKe} dari ${widget.argument.jumlahPertemuan}",
          color: Colors.white,
        ),
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: finish, color: Colors.white),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(20)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            StyledText("Materi",
              size: sp(14),
            ),
            SizedBox(height: width(10)),
            Container(
              width: double.infinity,
              height: width(40),
              padding: EdgeInsets.symmetric(
                horizontal: width(10),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(width(10)),
                border: Border.all(color: ColorUtils.primary),
              ),
              child: TextFormField(
                onChanged: (it) => _materi = it,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.words,
                focusNode: _materiFocus,
                onFieldSubmitted: (_){
                  _materiFocus.unfocus();
                },
                expands: true,
                maxLines: null,
                minLines: null,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "Contoh: Perkalian Kuadran",
                  hintStyle: StyledText.style(
                    color: ColorUtils.grey3333.withOpacity(0.4),
                    size: sp(14),
                  ),
                  border: InputBorder.none,
                ),
              ),
            ),

            SizedBox(height: width(25)),

            Row(
              children: <Widget>[
                Expanded(
                  child: StyledText("Jam Mulai",
                    size: sp(14),
                  ),
                ),
                SizedBox(
                  width: 35,
                ),
                Expanded(
                  child: StyledText("Jam Berakhir",
                    size: sp(14),
                  ),
                ),
              ],
            ),
            SizedBox(height: width(10)),
            Row(
              children: <Widget>[
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: ColorUtils.primary,
                            borderRadius: BorderRadius.circular(width(10)),
                          ),
                          child: Theme(
                            data: ThemeData(
                              textTheme: TextTheme(
                                body1: StyledText.style(
                                  size: width(18),
                                  color: Colors.white.withOpacity(0.75),
                                ),
                                headline: StyledText.style(
                                  size: width(24),
                                ),
                              ),
                              accentColor: Colors.white,
                            ),
                            child: NumberPicker.integer(
                              textMapper: (it) => it.length == 1 ? "0$it" : it,
                              initialValue: _startHour,
                              infiniteLoop: true,
                              minValue: 0,
                              maxValue: 23,
                              onChanged: (it) => setState(() => _startHour = it),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: width(5)),
                      SizedBox(width: width(5)),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: ColorUtils.primary,
                            borderRadius: BorderRadius.circular(width(10)),
                          ),
                          child: Theme(
                            data: ThemeData(
                              textTheme: TextTheme(
                                body1: StyledText.style(
                                  size: width(18),
                                  color: Colors.white.withOpacity(0.75),
                                ),
                                headline: StyledText.style(
                                  size: width(24),
                                ),
                              ),
                              accentColor: Colors.white,
                            ),
                            child: NumberPicker.integer(
                              textMapper: (it) => it.length == 1 ? "0$it" : it,
                              initialValue: _startMinutes,
                              infiniteLoop: true,
                              minValue: 0,
                              maxValue: 59,
                              onChanged: (it) => setState(() => _startMinutes = it),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 15,
                  height: 5,
                  margin: EdgeInsets.symmetric(
                    horizontal: 10,
                  ),
                  color: Colors.black,
                ),
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: ColorUtils.primary,
                            borderRadius: BorderRadius.circular(width(10)),
                          ),
                          child: Theme(
                            data: ThemeData(
                              textTheme: TextTheme(
                                body1: StyledText.style(
                                  size: width(18),
                                  color: Colors.white.withOpacity(0.75),
                                ),
                                headline: StyledText.style(
                                  size: width(24),
                                ),
                              ),
                              accentColor: Colors.white,
                            ),
                            child: NumberPicker.integer(
                              textMapper: (it) => it.length == 1 ? "0$it" : it,
                              initialValue: _endHour,
                              infiniteLoop: true,
                              minValue: 0,
                              maxValue: 23,
                              onChanged: (it) => setState(() => _endHour = it),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: width(5)),
                      SizedBox(width: width(5)),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: ColorUtils.primary,
                            borderRadius: BorderRadius.circular(width(10)),
                          ),
                          child: Theme(
                            data: ThemeData(
                              textTheme: TextTheme(
                                body1: StyledText.style(
                                  size: width(18),
                                  color: Colors.white.withOpacity(0.75),
                                ),
                                headline: StyledText.style(
                                  size: width(24),
                                ),
                              ),
                              accentColor: Colors.white,
                            ),
                            child: NumberPicker.integer(
                              textMapper: (it) => it.length == 1 ? "0$it" : it,
                              initialValue: _endMinutes,
                              infiniteLoop: true,
                              minValue: 0,
                              maxValue: 59,
                              onChanged: (it) => setState(() => _endMinutes = it),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),

            SizedBox(height: width(25)),

            StyledText("Tanggal",
              size: sp(14),
            ),
            SizedBox(height: width(10)),
            Container(
              width: double.infinity,
              height: width(40),
              padding: EdgeInsets.symmetric(
                horizontal: width(10),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(width(10)),
                border: Border.all(color: ColorUtils.primary),
              ),
              child: GestureDetector(
                onTap: (){
                  var now = new DateTime(DateTime.now().year,
                      DateTime.now().month,
                      DateTime.now().day, 0, 0, 0, 0);
                  openDatePicker(context: context, firstDate: now);
                },
                behavior: HitTestBehavior.opaque,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        controller: _dateController,
                        expands: true,
                        maxLines: null,
                        minLines: null,
                        enabled: false,
                        textInputAction: TextInputAction.next,
                        decoration: InputDecoration(
                          hintText: "dd/mm/yyyy",
                          hintStyle: StyledText.style(
                            color: ColorUtils.grey3333.withOpacity(0.4),
                            size: sp(14),
                          ),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    Icon(Icons.date_range, color: Colors.black.withOpacity(0.5)),
                  ],
                ),
              ),
            ),

            SizedBox(height: width(25)),
            MaterialButton(
              onPressed: () => _presenter.validate(_materi, _selectedDate, _startHour, _startMinutes,
                  _endHour, _endMinutes),
              color: ColorUtils.primary,
              minWidth: double.infinity,
              height: width(40),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(width(10)),
              ),
              child: StyledText(widget.argument.pertemuanKe == widget.argument.jumlahPertemuan ? "Buat" : "Lanjut",
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
      ),
    );
  }
}