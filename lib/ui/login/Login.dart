import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/ui/login/LoginDelegate.dart';
import 'package:halokes_les_guru/ui/login/LoginPresenter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends BaseState<Login> with LoginDelegate{
  LoginPresenter _presenter;

  FocusNode _usernameFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();

  String _username = "";
  String _password = "";

  @override
  void initState() {
    super.initState();
    _presenter = LoginPresenter(this, this);
  }

  @override
  void onSuccessLogin() {
    navigateTo(MyApp.ROUTE_HOME, singleTop: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: fullHeight(),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: width(230),
                    color: ColorUtils.primary,
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                      top: width(210),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(width(20)),
                        topLeft: Radius.circular(width(20)),
                      ),
                    ),
                    padding: EdgeInsets.all(width(20)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        StyledText("MASUK",
                          size: sp(16),
                          fontWeight: FontWeight.w600,
                        ),
                        StyledText("Selamat datang, silahkan masuk dengan akun anda",
                          size: sp(14),
                          fontWeight: FontWeight.w600,
                          color: ColorUtils.grey4e4e,
                        ),
                        SizedBox(height: width(50)),
                        Container(
                          width: double.infinity,
                          height: width(50),
                          decoration: BoxDecoration(
                            color: ColorUtils.greyf1f1,
                            borderRadius: BorderRadius.circular(width(5)),
                            border: Border.all(color: ColorUtils.greydede),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: width(10),
                          ),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.person_outline, color: ColorUtils.grey7070),
                              SizedBox(width: width(10)),
                              Expanded(
                                child: TextFormField(
                                  onChanged: (it) => _username = it,
                                  focusNode: _usernameFocus,
                                  keyboardType: TextInputType.emailAddress,
                                  textInputAction: TextInputAction.next,
                                  onFieldSubmitted: (_){
                                    _usernameFocus.unfocus();
                                    _passwordFocus.requestFocus();
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Username / Email"
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: width(20)),
                        Container(
                          width: double.infinity,
                          height: width(50),
                          decoration: BoxDecoration(
                            color: ColorUtils.greyf1f1,
                            borderRadius: BorderRadius.circular(width(5)),
                            border: Border.all(color: ColorUtils.greydede),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: width(10),
                          ),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.lock_outline, color: ColorUtils.grey7070),
                              SizedBox(width: width(10)),
                              Expanded(
                                child: TextFormField(
                                  onChanged: (it) => _password = it,
                                  obscureText: true,
                                  keyboardType: TextInputType.visiblePassword,
                                  textInputAction: TextInputAction.done,
                                  onFieldSubmitted: (_) => _passwordFocus.unfocus(),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Password",
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Spacer(),
                        MaterialButton(
                          onPressed: () => _presenter.executeLogin(_username, _password),
                          elevation: 0,
                          color: ColorUtils.primary,
                          minWidth: double.infinity,
                          height: width(40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(width(10)),
                          ),
                          child: StyledText("Masuk",
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: width(10)),
                        StyledText("Belum punya akun?",
                          size: sp(14),
                          color: ColorUtils.grey4e4e,
                        ),
                        SizedBox(height: width(10)),
                        MaterialButton(
                          onPressed: () => navigateTo(MyApp.ROUTE_REGISTER),
                          elevation: 0,
                          color: Colors.white,
                          minWidth: double.infinity,
                          height: width(40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(width(10)),
                            side: BorderSide(color: ColorUtils.primary),
                          ),
                          child: StyledText("Daftar",
                            color: ColorUtils.primary,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
