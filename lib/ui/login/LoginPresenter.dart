import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';
import 'package:halokes_les_guru/ui/login/LoginDelegate.dart';

class LoginPresenter extends BasePresenter {
  static const REQUEST_LOGIN = 1;

  final LoginDelegate _delegate;

  LoginPresenter(BaseState state, this._delegate) : super(state);

  void executeLogin(String username, String password){
    if(username == ""){
      state.alert(title: "Error", message: "Username masih kosong", positiveTitle: "Tutup");
      return;
    }

    if(password == ""){
      state.alert(title: "Error", message: "Password masih kosong", positiveTitle: "Tutup");
      return;
    }

    var params = {
      "username" : username,
      "password" : password,
    };

    generalRepo.executeLogin(REQUEST_LOGIN, params, (it) async {
      await AppPreference.saveUser(it.data);
      _delegate.onSuccessLogin();
    });
  }
}