import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends BaseState<SplashScreen> {

  @override
  void afterWidgetBuilt() {
    delay(500, () async {
      if(await AppPreference.getUser() == null){
        navigateTo(MyApp.ROUTE_LOGIN, singleTop: true);
      }else {
        navigateTo(MyApp.ROUTE_HOME, singleTop: true);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
