import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:halokes_les_guru/ancestor/BaseResponse.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:halokes_les_guru/ui/register/RegisterDelegate.dart';
import 'package:halokes_les_guru/ui/register/RegisterPresenter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends BaseState<Register> with RegisterDelegate{
  RegisterPresenter _presenter;

  FocusNode _namaLengkapFocus = FocusNode();
  FocusNode _asalSekolahFocus = FocusNode();
  FocusNode _emailFocus = FocusNode();
  FocusNode _usernameFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  FocusNode _ulangiPasswordFocus = FocusNode();

  String _namaLengkap = "";
  String _asalSekolah = "";
  String _email = "";
  String _username = "";
  String _password = "";
  String _ulangiPassword = "";

  @override
  void initState() {
    super.initState();
    _presenter = RegisterPresenter(this, this);
  }

  @override
  void onSuccessRegister(BaseResponse response) {
    Fluttertoast.showToast(msg: "Pendaftaran berhasil, silahkan login");
    finish();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: fullHeight(),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: width(100),
                    color: ColorUtils.primary,
                  ),
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                      top: width(80),
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(width(20)),
                        topLeft: Radius.circular(width(20)),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: width(20)),
                        Row(
                          children: <Widget>[
                            IconButton(icon: Icon(Icons.arrow_back),
                              onPressed: finish,
                            ),
                            SizedBox(height: width(10)),
                            StyledText("DAFTAR",
                              size: sp(16),
                              fontWeight: FontWeight.w600,
                            ),
                          ],
                        ),
                        SizedBox(height: width(20)),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: width(20)),
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: double.infinity,
                                height: width(50),
                                decoration: BoxDecoration(
                                  color: ColorUtils.greyf1f1,
                                  borderRadius: BorderRadius.circular(width(5)),
                                  border: Border.all(color: ColorUtils.greydede),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: width(10),
                                ),
                                child: TextFormField(
                                  onChanged: (it) => _namaLengkap = it,
                                  focusNode: _namaLengkapFocus,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.next,
                                  textCapitalization: TextCapitalization.words,
                                  onFieldSubmitted: (_){
                                    _namaLengkapFocus.unfocus();
                                    _asalSekolahFocus.requestFocus();
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Nama Lengkap",
                                  ),
                                ),
                              ),
                              SizedBox(height: width(20)),
                              Container(
                                width: double.infinity,
                                height: width(50),
                                decoration: BoxDecoration(
                                  color: ColorUtils.greyf1f1,
                                  borderRadius: BorderRadius.circular(width(5)),
                                  border: Border.all(color: ColorUtils.greydede),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: width(10),
                                ),
                                child: TextFormField(
                                  onChanged: (it) => _asalSekolah = it,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.next,
                                  focusNode: _asalSekolahFocus,
                                  onFieldSubmitted: (_){
                                    _asalSekolahFocus.unfocus();
                                    _emailFocus.requestFocus();
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Asal Sekolah",
                                  ),
                                ),
                              ),
                              SizedBox(height: width(20)),
                              Container(
                                width: double.infinity,
                                height: width(50),
                                decoration: BoxDecoration(
                                  color: ColorUtils.greyf1f1,
                                  borderRadius: BorderRadius.circular(width(5)),
                                  border: Border.all(color: ColorUtils.greydede),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: width(10),
                                ),
                                child: TextFormField(
                                  onChanged: (it) => _email = it,
                                  keyboardType: TextInputType.emailAddress,
                                  textInputAction: TextInputAction.next,
                                  focusNode: _emailFocus,
                                  onFieldSubmitted: (_){
                                    _emailFocus.unfocus();
                                    _usernameFocus.requestFocus();
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Email",
                                  ),
                                ),
                              ),
                              SizedBox(height: width(20)),
                              Container(
                                width: double.infinity,
                                height: width(50),
                                decoration: BoxDecoration(
                                  color: ColorUtils.greyf1f1,
                                  borderRadius: BorderRadius.circular(width(5)),
                                  border: Border.all(color: ColorUtils.greydede),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: width(10),
                                ),
                                child: TextFormField(
                                  onChanged: (it) => _username = it,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.next,
                                  focusNode: _usernameFocus,
                                  onFieldSubmitted: (_){
                                    _usernameFocus.unfocus();
                                    _passwordFocus.requestFocus();
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Username",
                                  ),
                                ),
                              ),
                              SizedBox(height: width(20)),
                              Container(
                                width: double.infinity,
                                height: width(50),
                                decoration: BoxDecoration(
                                  color: ColorUtils.greyf1f1,
                                  borderRadius: BorderRadius.circular(width(5)),
                                  border: Border.all(color: ColorUtils.greydede),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: width(10),
                                ),
                                child: TextFormField(
                                  onChanged: (it) => _password = it,
                                  keyboardType: TextInputType.visiblePassword,
                                  textInputAction: TextInputAction.next,
                                  obscureText: true,
                                  focusNode: _passwordFocus,
                                  onFieldSubmitted: (_){
                                    _passwordFocus.unfocus();
                                    _ulangiPasswordFocus.requestFocus();
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Password",
                                  ),
                                ),
                              ),
                              SizedBox(height: width(20)),
                              Container(
                                width: double.infinity,
                                height: width(50),
                                decoration: BoxDecoration(
                                  color: ColorUtils.greyf1f1,
                                  borderRadius: BorderRadius.circular(width(5)),
                                  border: Border.all(color: ColorUtils.greydede),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: width(10),
                                ),
                                child: TextFormField(
                                  onChanged: (it) => _ulangiPassword = it,
                                  keyboardType: TextInputType.visiblePassword,
                                  textInputAction: TextInputAction.next,
                                  obscureText: true,
                                  focusNode: _ulangiPasswordFocus,
                                  onFieldSubmitted: (_) => _ulangiPasswordFocus.unfocus(),
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Ulangi Password",
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(),
                        Padding(
                          padding: EdgeInsets.all(width(20)),
                          child: MaterialButton(
                            onPressed: () => _presenter.executeRegister(
                              _namaLengkap, _asalSekolah, _email, _username, _password, _ulangiPassword,
                            ),
                            color: ColorUtils.primary,
                            minWidth: double.infinity,
                            height: width(40),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(width(10)),
                            ),
                            child: StyledText("Daftar",
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
