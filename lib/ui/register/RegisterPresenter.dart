import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/ui/register/RegisterDelegate.dart';

class RegisterPresenter extends BasePresenter {
  static const REQUEST_REGISTER = 0;

  final RegisterDelegate _delegate;

  RegisterPresenter(BaseState state, this._delegate) : super(state);

  void executeRegister(String namaLengkap, String asalSekolah, String email, String username, String password, String ulangPassword){
    if(namaLengkap == ""){
      state.alert(title: "Error", message: "Nama masih kosong", positiveTitle: "Tutup");
      return;
    }

    if(asalSekolah == ""){
      state.alert(title: "Error", message: "Asal sekolah masih kosong", positiveTitle: "Tutup");
      return;
    }

    if(email == ""){
      state.alert(title: "Error", message: "Email masih kosong", positiveTitle: "Tutup");
      return;
    }

    if(username == ""){
      state.alert(title: "Error", message: "Username masih kosong", positiveTitle: "Tutup");
      return;
    }

    if(password == ""){
      state.alert(title: "Error", message: "Password masih kosong", positiveTitle: "Tutup");
      return;
    }

    if(ulangPassword == ""){
      state.alert(title: "Error", message: "Konfirmasi password masih kosong", positiveTitle: "Tutup");
      return;
    }

    if(password != ulangPassword){
      state.alert(title: "Error", message: "Password berbeda dengan konfirmasi password", positiveTitle: "Tutup");
      return;
    }

    var params = <String, dynamic>{
      "email" : email,
      "username" : username,
      "password" : password,
      "nama_guru" : namaLengkap,
      "guru_sekolah" : asalSekolah
    };

    generalRepo.executeRegister(REQUEST_REGISTER, params, _delegate.onSuccessRegister);
  }
}