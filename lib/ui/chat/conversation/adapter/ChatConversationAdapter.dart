import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/database/database.dart';
import 'package:halokes_les_guru/database/entities/Conversation.dart';
import 'package:halokes_les_guru/extension/Size.dart';
import 'package:halokes_les_guru/extension/Date.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:halokes_les_guru/utils/ImageUtils.dart';

class ChatConversationItem extends StatelessWidget {
  final Conversation conversation;

  ChatConversationItem({this.conversation});

  @override
  Widget build(BuildContext context) {
    if(conversation.sender == ConversationEntity.ME){
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              SizedBox(width: adaptiveWidth(context, 25)),
              Flexible(
                child: Container(
                  padding: EdgeInsets.all(adaptiveWidth(context, 15)),
                  decoration: BoxDecoration(
                    color: ColorUtils.primary,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(adaptiveWidth(context, 10)),
                      topLeft: Radius.circular(adaptiveWidth(context, 10)),
                      bottomLeft: Radius.circular(adaptiveWidth(context, 10)),
                    ),
                  ),
                  child: StyledText(conversation.conversation,
                    size: sp(context, 12),
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: adaptiveWidth(context, 5)),
          Row(
            children: <Widget>[
              Spacer(),
              StyledText(_getChatTime(),
                size: sp(context, 11),
                color: ColorUtils.grey7e7e,
              ),
              SizedBox(width: adaptiveWidth(context, 5)),
              _getStatusIcon(context),
            ],
          ),
          SizedBox(height: adaptiveWidth(context, 15)),
        ],
      );
    }

    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Flexible(
              child: Container(
                padding: EdgeInsets.all(adaptiveWidth(context, 15)),
                decoration: BoxDecoration(
                  color: ColorUtils.greye7e7,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(adaptiveWidth(context, 10)),
                    topLeft: Radius.circular(adaptiveWidth(context, 10)),
                    bottomRight: Radius.circular(adaptiveWidth(context, 10)),
                  ),
                ),
                child: StyledText(conversation.conversation,
                  size: sp(context, 12),
                ),
              ),
            ),
            SizedBox(width: adaptiveWidth(context, 25)),
          ],
        ),
        SizedBox(height: adaptiveWidth(context, 5)),
        Row(
          children: <Widget>[
            StyledText(_getChatTime(),
              size: sp(context, 11),
              color: ColorUtils.grey7e7e,
            ),
          ],
        ),
        SizedBox(height: adaptiveWidth(context, 15)),
      ],
    );
  }

  String _getChatTime(){
    DateTime lastChat = DateTime.fromMillisecondsSinceEpoch(conversation.sendAt.toInt());
    DateTime today = DateTime.now();
    int differDays = today.weekday - lastChat.weekday;

    if(today.year == lastChat.year && today.month == lastChat.month){
      if(differDays == 1){
        return "Kemarin ${fromMillisecond("HH:mm", conversation.sendAt.toInt())}";
      }else if(differDays == 0){
        return "Hari ini ${fromMillisecond("HH:mm", conversation.sendAt.toInt())}";
      }else {
        return fromMillisecond("dd/MM HH:mm", conversation.sendAt.toInt());
      }
    }else {
      return fromMillisecond("dd/MM/yy", conversation.sendAt.toInt());
    }

  }

  SvgPicture _getStatusIcon(BuildContext context){
    if(conversation.conversationStatus == ConversationEntity.STATUS_SEND){
      return SvgPicture.asset(ImageUtils.ic_pending,
        width: adaptiveWidth(context, 10),
        height: adaptiveWidth(context, 10),
        fit: BoxFit.cover,
      );
    }else if(conversation.conversationStatus == ConversationEntity.STATUS_RECEIVED){
      return SvgPicture.asset(ImageUtils.ic_received_by_server,
        width: adaptiveWidth(context, 10),
        height: adaptiveWidth(context, 10),
        fit: BoxFit.cover,
      );
    }else if(conversation.conversationStatus == ConversationEntity.STATUS_SENT){
      return SvgPicture.asset(ImageUtils.ic_sent,
        width: adaptiveWidth(context, 10),
        height: adaptiveWidth(context, 10),
        fit: BoxFit.cover,
      );
    }else {
      return SvgPicture.asset(ImageUtils.ic_read,
        width: adaptiveWidth(context, 10),
        height: adaptiveWidth(context, 10),
        fit: BoxFit.cover,
      );
    }
  }
}
