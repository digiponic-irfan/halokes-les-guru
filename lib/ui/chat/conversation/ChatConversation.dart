import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseLifecycleState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/database/database.dart';
import 'package:halokes_les_guru/network/response/general/ChatUserResponse.dart';
import 'package:halokes_les_guru/provider/ChatLastSeenProvider.dart';
import 'package:halokes_les_guru/provider/TypingProvider.dart';
import 'package:halokes_les_guru/ui/chat/conversation/ChatConversationPresenter.dart';
import 'package:halokes_les_guru/ui/chat/conversation/adapter/ChatConversationAdapter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:halokes_les_guru/utils/ImageUtils.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:mcnmr_common_ext/FutureDelayed.dart';
import 'package:provider/provider.dart';

class ChatConversation extends StatefulWidget {
  final ChatUser argument;

  ChatConversation({@required this.argument});

  @override
  _ChatConversationState createState() => _ChatConversationState();
}

class _ChatConversationState extends BaseLifecycleState<ChatConversation> {
  ChatConversationPresenter _presenter;
  ChatLastSeenProvider _lastSeenProvider;

  String _message = "";
  TextEditingController _chatController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _presenter = ChatConversationPresenter(this, widget.argument)..watchUser();
    _lastSeenProvider = Provider.of<ChatLastSeenProvider>(context, listen: false);
  }

  @override
  void afterWidgetBuilt() async {
    if(!_lastSeenProvider.isLastSeenExists(widget.argument.idUrl)){
      if(widget.argument.displayOnline != ""){
        if(widget.argument.displayOnline != "Online"){
          _lastSeenProvider.setLastSeen(widget.argument.idUrl, widget.argument.displayOnline);
        }else {
          _lastSeenProvider.someoneOnline(widget.argument.idUrl);
        }
      }else {
        _presenter.executeGetLastSeen(widget.argument.idUrl, (lastSeen){
          if(lastSeen == "Online"){
            _lastSeenProvider.someoneOnline(widget.argument.idUrl);
          }else {
            _lastSeenProvider.setLastSeen(widget.argument.idUrl, lastSeen);
          }
        });
      }
    }

    _presenter.readMessage();
  }

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void onResume() {
    _presenter.watchUser();
    _presenter.readMessage();
  }

  @override
  void onPause() => _presenter.unwatch();

  @override
  void onDestroy() => _presenter.unwatch();

  @override
  void onRequestTimeOut(int typeRequest) => delay(5000, () => _presenter.executeGetLastSeen(widget.argument.idUrl, (lastSeen){
    if(lastSeen == "Online"){
      _lastSeenProvider.someoneOnline(widget.argument.idUrl);
    }else {
      _lastSeenProvider.setLastSeen(widget.argument.idUrl, lastSeen);
    }
  }));

  @override
  void onNoConnection(int typeRequest) => delay(5000, () => _presenter.executeGetLastSeen(widget.argument.idUrl, (lastSeen){
    if(lastSeen == "Online"){
      _lastSeenProvider.someoneOnline(widget.argument.idUrl);
    }else {
      _lastSeenProvider.setLastSeen(widget.argument.idUrl, lastSeen);
    }
  }));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.greyf1f1,
      body: Column(
        children: <Widget>[
          SizedBox(height: MediaQuery.of(context).padding.top),
          Material(
            elevation: 2,
            child: Container(
              width: double.infinity,
              height: kToolbarHeight,
              child: Row(
                children: <Widget>[
                  IconButton(icon: Icon(Icons.arrow_back_ios),
                    onPressed: finish,
                    color: ColorUtils.primary,
                  ),
                  Container(
                    height: 32,
                    width: 32,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage(ImageUtils.image_not_found),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: width(10)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      StyledText(widget.argument.name,
                        size: 14,
                        color: ColorUtils.primary,
                        fontWeight: FontWeight.w600,
                      ),
                      Consumer2<ChatLastSeenProvider, TypingProvider>(
                        builder: (_, lastSeen, typing, __) => StyledText(
                          _getUserStatus(lastSeen, typing),
                          size: 12,
                          color: ColorUtils.primary,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 2),
          Expanded(
            child: StreamBuilder<List<Conversation>>(
              stream: _presenter.conversationStream,
              builder: (_, snapshot){
                if(snapshot.hasData){
                  return ListView.builder(
                    cacheExtent: 2 * fullHeight(),
                    padding: EdgeInsets.only(
                      top: width(15),
                      left: width(15),
                      right: width(15),
                    ),
                    itemCount: snapshot.data.length,
                    reverse: true,
                    itemBuilder: (_, index) => ChatConversationItem(
                      conversation: snapshot.data[index],
                    ),
                  );
                }

                return Container();
              },
            ),
          ),
          SizedBox(height: width(15)),
          Row(
            children: <Widget>[
              SizedBox(width: width(25)),
              Expanded(
                child: Container(
                  constraints: BoxConstraints(
                    minHeight: width(50),
                  ),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(width(15)),
                  ),
                  child: TextFormField(
                    onChanged: (it){
                      _message = it;
                      _presenter.typing();
                    },
                    controller: _chatController,
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.multiline,
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: null,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.all(width(15)),
                      hintText: "Tulis pesan",
                    ),
                  ),
                ),
              ),
              SizedBox(width: width(15)),
              Material(
                color: ColorUtils.primary,
                shape: CircleBorder(),
                child: InkWell(
                  onTap: (){
                    _presenter.sendMessage(_message);
                    _chatController.text = "";
                  },
                  child: Container(
                    width: width(50),
                    height: width(50),
                    alignment: Alignment.center,
                    child: Icon(Icons.send, color: Colors.white, size: width(24)),
                  ),
                ),
              ),
              SizedBox(width: width(25)),
            ],
          ),
          SizedBox(height: width(25)),
        ],
      ),
    );
  }

  String _getUserStatus(ChatLastSeenProvider chatProvider, TypingProvider typingProvider){
    if(typingProvider.isTyping(widget.argument.idUrl)){
      return "Sedang mengetik...";
    }

    if(chatProvider.isLastSeenExists(widget.argument.idUrl)){
      return chatProvider.getOnlineStatus(widget.argument.idUrl);
    }

    return "";
  }
}
