import 'package:flutter/material.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/Size.dart';
import 'package:halokes_les_guru/network/response/general/ChatUserResponse.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:halokes_les_guru/utils/ImageUtils.dart';
import 'package:shimmer/shimmer.dart';

class ChatSearchItem extends StatelessWidget {
  final ChatUser data;
  final Function(ChatUser) onSelected;

  ChatSearchItem({@required this.data, @required this.onSelected})
      : assert(data != null), assert(onSelected != null);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onSelected(data),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(adaptiveWidth(context, 15)),
            child: Row(
              children: <Widget>[
                Container(
                  width: adaptiveWidth(context, 48),
                  height: adaptiveWidth(context, 48),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage(ImageUtils.image_not_found),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: adaptiveWidth(context, 15)),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      StyledText(data.name,
                        size: sp(context, 14),
                        fontWeight: FontWeight.w600,
                      ),
                      SizedBox(height: adaptiveWidth(context, 3)),
                      StyledText("@${data.username}",
                        size: sp(context, 12),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        color: ColorUtils.grey7e7e,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            width: double.infinity,
            height: 1,
            color: ColorUtils.greyf1f1,
          ),
        ],
      ),
    );
  }
}

class ShimmerChatSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          padding: EdgeInsets.all(adaptiveWidth(context, 15)),
          child: Row(
            children: <Widget>[
              Shimmer.fromColors(
                child: Container(
                  width: adaptiveWidth(context, 48),
                  height: adaptiveWidth(context, 48),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey[200],
                  ),
                ),
                baseColor: Colors.grey[200],
                highlightColor: Colors.white,
              ),
              SizedBox(width: adaptiveWidth(context, 15)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Shimmer.fromColors(
                    child: Container(
                      width: adaptiveWidth(context, 200),
                      height: sp(context, 14),
                      color: Colors.grey[200],
                    ),
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.white,
                  ),
                  SizedBox(height: adaptiveWidth(context, 3)),
                  Shimmer.fromColors(
                    child: Container(
                      width: adaptiveWidth(context, 80),
                      height: sp(context, 12),
                      color: Colors.grey[200],
                    ),
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.white,
                  ),
                ],
              ),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          height: 1,
          color: ColorUtils.greyf1f1,
        ),
      ],
    );
  }
}

