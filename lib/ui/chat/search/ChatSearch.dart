import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/network/response/general/ChatUserResponse.dart';
import 'package:halokes_les_guru/ui/chat/search/ChatSearchPresenter.dart';
import 'package:halokes_les_guru/ui/chat/search/adapter/ChatSearchAdapter.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';

class ChatSearch extends StatefulWidget {
  @override
  _ChatSearchState createState() => _ChatSearchState();
}

class _ChatSearchState extends BaseState<ChatSearch> {
  ChatSearchPresenter _presenter;
  RequestWrapper<ChatUserResponse> _userWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = ChatSearchPresenter(this);
  }

  @override
  void afterWidgetBuilt() {
    _presenter.executeGetChatUser(_userWrapper);
  }

  @override
  void shouldShowLoading(int typeRequest) {}

  @override
  void shouldHideLoading(int typeRequest) {}

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => finish(),
          ),
          title: StyledText("Cari Kontak", color: Colors.white),
        ),
        body: RequestWrapperWidget<ChatUserResponse>(
          requestWrapper: _userWrapper,
          placeholder: (_) => ListView.builder(
            itemCount: 5,
            itemBuilder: (_, index) => ShimmerChatSearch(),
          ),
          builder: (_, data){
            if(data.data.length > 0){
              return ListView.builder(
                itemCount: data.data.length,
                itemBuilder: (_, index) => ChatSearchItem(data: data.data[index],
                  onSelected: _onUserSelected,
                ),
              );
            }

            return Center(
              child: StyledText(data.message,
                fontWeight: FontWeight.w600,
                size: sp(16),
              ),
            );
          },
        ),
      ),
    );
  }

  void _onUserSelected(ChatUser user) => navigateTo(MyApp.ROUTE_CHAT_CONVERSATION, arguments: user);
}
