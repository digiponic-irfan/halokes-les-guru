import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/network/response/general/ChatUserResponse.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class ChatSearchPresenter extends BasePresenter {
  static const REQUEST_GET_USER = 0;

  ChatSearchPresenter(BaseState state) : super(state);

  void executeGetChatUser(RequestWrapper<ChatUserResponse> wrapper){
    wrapper.doRequest();
    generalRepo.executeGetChatUser(REQUEST_GET_USER, wrapper.finishRequest);
  }
}