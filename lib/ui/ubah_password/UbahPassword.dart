import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:halokes_les_guru/ancestor/BaseResponse.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ui/ubah_password/UbahPasswordDelegate.dart';
import 'package:halokes_les_guru/ui/ubah_password/UbahPasswordPresenter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';

class UbahPassword extends StatefulWidget {
  @override
  _UbahPasswordState createState() => _UbahPasswordState();
}

class _UbahPasswordState extends BaseState<UbahPassword> with UbahPasswordDelegate{
  UbahPasswordPresenter _presenter;

  String _passwordLama = "";
  String _passwordBaru = "";
  String _konfirmasiPasswordBaru = "";

  FocusNode _passwordLamaFocus = FocusNode();
  FocusNode _passwordBaruFocus = FocusNode();
  FocusNode _konfirmasiPasswordBaruFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    _presenter = UbahPasswordPresenter(this, this);
  }

  @override
  void onSuccessUpdate(BaseResponse response) {
    Fluttertoast.showToast(msg: "Sukses ubah password");
    finish();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorUtils.greyf1f1,
        appBar: AppBar(
          title: StyledText("Ubah Password"),
          leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: finish, color: Colors.white),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(width(20)),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(width(15)),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(width(10)),
                ),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      onChanged: (it) => _passwordLama = it,
                      keyboardType: TextInputType.visiblePassword,
                      textInputAction: TextInputAction.next,
                      obscureText: true,
                      focusNode: _passwordLamaFocus,
                      onFieldSubmitted: (_){
                        _passwordLamaFocus.unfocus();
                        _passwordBaruFocus.requestFocus();
                      },
                      style: StyledText.style(
                        size: sp(14),
                      ),
                      decoration: InputDecoration(
                        labelText: "Password lama",
                        labelStyle: StyledText.style(
                          color: ColorUtils.grey7e7e,
                          size: sp(14),
                        ),
                      ),
                    ),
                    SizedBox(height: width(10)),
                    TextFormField(
                      onChanged: (it) => _passwordBaru = it,
                      keyboardType: TextInputType.visiblePassword,
                      textInputAction: TextInputAction.next,
                      obscureText: true,
                      focusNode: _passwordBaruFocus,
                      onFieldSubmitted: (_){
                        _passwordBaruFocus.unfocus();
                        _konfirmasiPasswordBaruFocus.requestFocus();
                      },
                      style: StyledText.style(
                        size: sp(14),
                      ),
                      decoration: InputDecoration(
                        labelText: "Password baru",
                        labelStyle: StyledText.style(
                          color: ColorUtils.grey7e7e,
                          size: sp(14),
                        ),
                      ),
                    ),
                    SizedBox(height: width(10)),
                    TextFormField(
                      onChanged: (it) => _konfirmasiPasswordBaru = it,
                      keyboardType: TextInputType.visiblePassword,
                      textInputAction: TextInputAction.done,
                      obscureText: true,
                      focusNode: _konfirmasiPasswordBaruFocus,
                      onFieldSubmitted: (_){
                        _konfirmasiPasswordBaruFocus.unfocus();
                      },
                      style: StyledText.style(
                        size: sp(14),
                      ),
                      decoration: InputDecoration(
                        labelText: "Konfirmasi password baru",
                        labelStyle: StyledText.style(
                          color: ColorUtils.grey7e7e,
                          size: sp(14),
                        ),
                      ),
                    ),
                    SizedBox(height: width(10)),
                    MaterialButton(
                      onPressed: () => _presenter.executeUbahPassword(_passwordLama, _passwordBaru, _konfirmasiPasswordBaru),
                      elevation: 0,
                      color: ColorUtils.primary,
                      minWidth: double.infinity,
                      height: width(40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width(10)),
                      ),
                      child: StyledText("Masuk",
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )
    );
  }

}
