import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/ui/ubah_password/UbahPasswordDelegate.dart';

class UbahPasswordPresenter extends BasePresenter {
  static const REQUEST_CHANGE_PASSWORD = 0;

  final UbahPasswordDelegate _delegate;
  UbahPasswordPresenter(BaseState state, this._delegate) : super(state);

  void executeUbahPassword(String oldPassword, String newPassword, String confirmPassword) async {
    if(oldPassword == ""){
      state.alert(title: "Error", message: "Password lama wajib diisi", positiveTitle: "Tutup");
      return;
    }

    if(newPassword == ""){
      state.alert(title: "Error", message: "Password baru wajib diisi", positiveTitle: "Tutup");
      return;
    }

    if(confirmPassword == ""){
      state.alert(title: "Error", message: "Konfirmasi Password wajib diisi", positiveTitle: "Tutup");
      return;
    }

    if(newPassword != confirmPassword){
      state.alert(title: "Error", message: "Password baru dan Konfirmasi Password tidak sama", positiveTitle: "Tutup");
      return;
    }

    var params = <String, dynamic>{
      "currentpassword" : oldPassword,
      "newpassword" : newPassword,
    };

    generalRepo.executeChangePassword(REQUEST_CHANGE_PASSWORD, params, _delegate.onSuccessUpdate);
  }
}