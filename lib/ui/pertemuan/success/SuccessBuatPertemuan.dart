import 'package:flutter_svg/flutter_svg.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:halokes_les_guru/utils/ImageUtils.dart';

class SuccessBuatPertemuan extends StatefulWidget {
  @override
  _SuccessBuatPertemuanState createState() => _SuccessBuatPertemuanState();
}

class _SuccessBuatPertemuanState extends BaseState<SuccessBuatPertemuan> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        body: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SvgPicture.asset(ImageUtils.vector_success_buat_jadwal),
              SizedBox(height: width(60)),
              StyledText("Jadwal berhasil dibuat",
                size: sp(16),
                fontWeight: FontWeight.w600,
              ),
              StyledText("Admin akan memproses jadwal kamu",
                size: sp(14),
              ),
              SizedBox(height: width(100)),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: width(20),
                ),
                child: MaterialButton(
                  onPressed: () => navigateTo(MyApp.ROUTE_HOME, singleTop: true),
                  elevation: 0,
                  color: ColorUtils.primary,
                  minWidth: double.infinity,
                  height: width(40),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(width(10)),
                  ),
                  child: StyledText("Selesai",
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      onWillPop: () async {
        navigateTo(MyApp.ROUTE_HOME, singleTop: true);
        return false;
      },
    );
  }
}
