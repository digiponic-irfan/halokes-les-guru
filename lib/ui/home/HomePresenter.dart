import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/network/request/SocketChatRepository.dart';
import 'package:halokes_les_guru/network/response/socket_chat/AskDelayedMessageResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/AskLatestMessageStatusResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/ServerReceivedYourMessageResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/SomeoneOfflineResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/SomeoneOnlineResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/SomeoneSendMessage.dart';
import 'package:halokes_les_guru/network/response/socket_chat/SomeoneTypingResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/TargetReadYourMessageResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/TargetReceivedYourMessageResponse.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';
import 'package:halokes_les_guru/provider/ChatLastSeenProvider.dart';
import 'package:halokes_les_guru/provider/TypingProvider.dart';

class HomePresenter extends BasePresenter {
  static const REQUEST_INSERT_BATCH_DELAYED_MESSAGE = 0;
  static const REQUEST_GET_SCHEDULE = 1;

  bool _isFirstTimeConnect = true;

  ChatLastSeenProvider _lastSeenProvider;
  TypingProvider _typingProvider;

  HomePresenter(BaseState state) : super(state);

  void connectToSocketServer(ChatLastSeenProvider lastSeenProvider, TypingProvider typingProvider) async {
    this._lastSeenProvider = lastSeenProvider;
    this._typingProvider = typingProvider;

    _registerEventHandler();
    chatSocketRepo.connect(header: {
      "x-halokes-user-url": (await AppPreference.getUser()).idUserUrl,
      "x-halokes-user-role": "teacher",
    });
  }

  void disconnectSocketServer(){
    chatSocketRepo.disconnect();
  }

  void goToBackground(){
    chatSocketRepo.emit(SocketChatRepository.ACTION_GO_TO_BACKGROUND);
  }

  void goToForeground(){
    chatSocketRepo.emit(SocketChatRepository.ACTION_GO_TO_FOREGROUND);
  }

  void _registerEventHandler(){
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_CONNECT, _onConnect);
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_SOMEONE_ONLINE, _onSomeoneOnline);
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_SOMEONE_OFFLINE, _onSomeoneOffline);
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_SOMEONE_TYPING, _onSomeoneTyping);
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_SOMEONE_SEND_MESSAGE, _onSomeoneSendMessage);
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_TARGET_RECEIVE_YOUR_MESSAGE, _onTargetReceiveYourMessage);
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_TARGET_RECEIVE_YOUR_DELAYED_MESSAGE, _onTargetReceiveYourDelayedMessage);
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_TARGET_READ_YOUR_MESSAGE, _onTargetReadYourMessage);
    chatSocketRepo.addEventListener(SocketChatRepository.EVENT_DISCONNECT, _onDisconnect);
  }

  void _onConnect(dynamic data) async {
    print("Socket event: Connected");

    _askForLatestMessageStatus();

    if(_isFirstTimeConnect){
      _isFirstTimeConnect = false;
      await _sendUndeliveredMessage();
    }

    chatSocketRepo.emitWithCallback(SocketChatRepository.ACTION_ASK_FOR_DELAYED_MESSAGE, {}, (data) async {
      await database.receiveDelayedMessage(AskDelayedMessageResponse.fromJson(data));
    });
  }

  Future<void> _askForLatestMessageStatus() async {
    var unreadUnsentMessage = await database.unreadUnsentMessage();
    if(unreadUnsentMessage.length > 0){
      var data = List<Map<String, dynamic>>();
      unreadUnsentMessage.forEach((it){
        var message = {
          "conversation_id" : it.conversationId,
        };
        data.add(message);
      });
      chatSocketRepo.emitWithCallback(SocketChatRepository.ACTION_ASK_FOR_UPDATED_MESSAGE_REPORT,
        {"data" : data}, (data) async {
          await database.updateStatusConversation(AskLatestMessageStatusResponse.fromJson(data));
        },
      );
    }
  }

  Future<void> _sendUndeliveredMessage() async {
    var undeliveredMessage = await database.undeliveredMessage();
    undeliveredMessage.forEach((it){
      var data = {
        "conversation_id" : it.conversationId,
        "conversation" : it.conversation,
        "send_at" : it.sendAt,
        "target_url_id" : it.interlocutorId,
      };

      chatSocketRepo.emitWithCallback(SocketChatRepository.ACTION_SEND_MESSAGE, data, (data) async {
        await database.yourMessageReceivedByServer(ServerReceivedYourMessageResponse.fromJson(data));
      });
    });
  }

  void _onSomeoneOnline(dynamic data){
    SomeoneOnlineResponse response = SomeoneOnlineResponse.fromJson(data);
    print("Socket event: Someone online");

    _lastSeenProvider.someoneOnline(response.idUserUrl);
  }

  void _onSomeoneOffline(dynamic data){
    SomeoneOfflineResponse response = SomeoneOfflineResponse.fromJson(data);
    print("Socket event: Someone offline");

    _lastSeenProvider.someoneOffline(response.idUserUrl, response.lastLogout);
  }

  void _onSomeoneTyping(dynamic data){
    SomeoneTypingResponse response = SomeoneTypingResponse.fromJson(data);
    print("Socket event: Someone typing");

    _typingProvider.someoneTyping(response.fromUrlId);
  }

  void _onSomeoneSendMessage(dynamic data) async {
    SomeoneSendMessageResponse response = SomeoneSendMessageResponse.fromJson(data);
    print("Socket event: Someone send message");

    await database.receiveMessage(response);
    _typingProvider.remove(response.senderUrlId);

    var emitData = <String, dynamic>{
      "sender_url_id" : response.senderUrlId,
      "conversation_id" : response.conversationId,
    };
    chatSocketRepo.emit(SocketChatRepository.ACTION_RECEIVING_MESSAGE, emitData);

    if(response.senderUrlId == chatSocketRepo.watchedUserId){
      chatSocketRepo.emit(SocketChatRepository.ACTION_READ_MESSAGE, {"sender_id" : response.senderUrlId});
      await database.readMessage(response.senderUrlId);
    }
  }

  void _onTargetReceiveYourMessage(dynamic data) async {
    print("Socket event: Target receive your message");
    await database.yourMessageReceivedByTarget(TargetReceivedYourMessageResponse.fromJson(data));
  }

  void _onTargetReceiveYourDelayedMessage(dynamic data) async {
    print("Socket event: Target receive your delayed message");
    await database.yourMessageReceivedByTarget(TargetReceivedYourMessageResponse.fromJson(data));
  }

  void _onTargetReadYourMessage(dynamic data) async {
    print("Socket event: Target read your message");
    await database.yourMessageReadByTarget(TargetReadYourMessageResponse.fromJson(data));
  }

  void _onDisconnect(dynamic data){
    print("Socket event: Disconnected");
    _lastSeenProvider.clear();
  }
}