import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseLifecycleState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/provider/ChatLastSeenProvider.dart';
import 'package:halokes_les_guru/provider/TypingProvider.dart';
import 'package:halokes_les_guru/ui/home/HomePresenter.dart';
import 'package:halokes_les_guru/ui/home/fragment/chat/FragmentChat.dart';
import 'package:halokes_les_guru/ui/home/fragment/explore/FragmentExplore.dart';
import 'package:halokes_les_guru/ui/home/fragment/profile/FragmentProfile.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends BaseLifecycleState<Home> {
  HomePresenter _presenter;

  var _currentIndex = 0;

  var _content = [
    FragmentExplore(),
    //FragmentChat(),
    FragmentProfile()
  ];

  @override
  void initState() {
    super.initState();
    _presenter = HomePresenter(this);
  }

  @override
  void afterWidgetBuilt() => _presenter.connectToSocketServer(
    Provider.of<ChatLastSeenProvider>(context, listen: false),
    Provider.of<TypingProvider>(context, listen: false),
  );

  @override
  void onResume() => _presenter.goToForeground();

  @override
  void onPause() => _presenter.goToBackground();

  @override
  void onDestroy() => _presenter.disconnectSocketServer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: _content,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (it) => setState(() => _currentIndex = it),
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: ColorUtils.primary,
        unselectedItemColor: Colors.black.withOpacity(0.5),
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.explore), title: StyledText("Explore",
            color: _currentIndex == 0 ? ColorUtils.primary : Colors.black.withOpacity(0.5),
          )),
          /**BottomNavigationBarItem(icon: Icon(Icons.chat), title: StyledText("Chat",
            color: _currentIndex == 1 ? ColorUtils.primary : Colors.black.withOpacity(0.5),
          )),*/
          BottomNavigationBarItem(icon: Icon(Icons.person), title: StyledText("Profile",
            color: _currentIndex == 1 ? ColorUtils.primary : Colors.black.withOpacity(0.5),
          )),
        ],
      ),
    );
  }
}
