import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/network/response/general/JadwalMengajarResponse.dart';
import 'package:halokes_les_guru/network/response/general/MapelResponse.dart';
import 'package:halokes_les_guru/ui/home/fragment/explore/FragmentExploreDelegate.dart';
import 'package:halokes_les_guru/ui/home/fragment/explore/FragmentExplorePresenter.dart';
import 'package:halokes_les_guru/ui/home/fragment/explore/adapter/JadwalMengajarAdapter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';

class FragmentExplore extends StatefulWidget {
  @override
  _FragmentExploreState createState() => _FragmentExploreState();
}

class _FragmentExploreState extends BaseState<FragmentExplore> with FragmentExploreDelegate{
  FragmentExplorePresenter _presenter;
  RequestWrapper<JadwalMengajarResponse> _jadwalWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = FragmentExplorePresenter(this, this);
  }

  @override
  void afterWidgetBuilt() {
    _presenter.executeGetJadwal(_jadwalWrapper);
  }

  @override
  void onSuccessGetMapel(MapelResponse response) =>
      navigateTo(MyApp.ROUTE_BUAT_JADWAL_MENGAJAR, arguments: response);

  @override
  void shouldShowLoading(int typeRequest) {
    if(typeRequest != FragmentExplorePresenter.REQUEST_GET_JADWAL){
      super.shouldShowLoading(typeRequest);
    }
  }

  @override
  void shouldHideLoading(int typeRequest) {
    if(typeRequest != FragmentExplorePresenter.REQUEST_GET_JADWAL){
      super.shouldHideLoading(typeRequest);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: Material(
        color: ColorUtils.primary,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(width(8)),
        ),
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          onTap: () => _presenter.executeGetMapel(),
          child: Container(
            width: width(50),
            height: width(50),
            alignment: Alignment.center,
            child: Icon(Icons.add, color: Colors.white),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AppBar(
              elevation: 0,
              title: Container(
                height: kToolbarHeight - 8,
                width: kToolbarHeight - 8,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  shape: BoxShape.circle,
                ),
              ),
            ),
            Stack(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: width(150),
                  color: ColorUtils.primary,
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.only(
                    top: width(80),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(width(20)),
                      topLeft: Radius.circular(width(20)),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: width(70)),
                      RequestWrapperWidget<JadwalMengajarResponse>(
                        requestWrapper: _jadwalWrapper,
                        placeholder: (_) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                top: width(20),
                                left: width(20),
                                right: width(20),
                                bottom: width(10),
                              ),
                              child: StyledText("Jadwal Mengajar",
                                size: sp(16),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            ListView.builder(
                              itemCount: 3,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              padding: EdgeInsets.zero,
                              itemBuilder: (_, index) => ShimmerJadwalMengajar(index: index, itemCount: 3),
                            )
                          ],
                        ),
                        builder: (_, data) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                top: width(20),
                                left: width(20),
                                right: width(20),
                                bottom: width(10),
                              ),
                              child: StyledText(data.data.length > 0 ? "Jadwal Mengajar" : "Belum ada jadwal mengajar",
                                size: sp(16),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            ListView.builder(
                              itemCount: data.data.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              padding: EdgeInsets.zero,
                              itemBuilder: (_, index) => JadwalMengajarItem(index: index,
                                itemCount: data.data.length,
                                data: data.data[index],
                                onSelected: _onJadwalSelected,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: width(40)),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(width(20)),
                    boxShadow: [
                      BoxShadow(
                        color: ColorUtils.primary.withOpacity(0.25),
                        blurRadius: 8.0,
                        spreadRadius: 4.0,
                      )
                    ],
                  ),
                  margin: EdgeInsets.only(
                    left: width(15),
                    right: width(15),
                    top: width(30),
                  ),
                  padding: EdgeInsets.symmetric(
                    vertical: width(15),
                    horizontal: width(25),
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            StyledText("Saldo",
                              size: width(14),
                              color: ColorUtils.primary,
                              fontWeight: FontWeight.w600,
                            ),
                            StyledText("Rp. 450.000",
                              size: width(20),
                              fontWeight: FontWeight.bold,
                              color: ColorUtils.primary,
                            ),
                            SizedBox(height: width(8)),
                            StyledText("April 2020",
                              size: width(12),
                              color: ColorUtils.grey7e7e,
                              fontWeight: FontWeight.w600,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: width(15)),
                      Material(
                        color: ColorUtils.primary,
                        shape: CircleBorder(),
                        clipBehavior: Clip.antiAlias,
                        child: InkWell(
                          onTap: (){},
                          child: Container(
                            height: width(60),
                            width: width(60),
                            alignment: Alignment.center,
                            child: Icon(Icons.account_balance_wallet, color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _onJadwalSelected(JadwalMengajarData data) =>
      navigateTo(MyApp.ROUTE_DETAIL_JADWAL, arguments: data);
}
