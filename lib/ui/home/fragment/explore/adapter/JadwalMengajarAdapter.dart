import 'package:flutter/material.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/Kelas.dart';
import 'package:halokes_les_guru/extension/Size.dart';
import 'package:halokes_les_guru/network/response/general/JadwalMengajarResponse.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

class JadwalMengajarItem extends StatelessWidget {
  final int index;
  final int itemCount;
  final JadwalMengajarData data;
  final Function(JadwalMengajarData) onSelected;

  JadwalMengajarItem({@required this.index, @required this.itemCount, @required this.data, @required this.onSelected});

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        onTap: () => onSelected(data),
        child: IntrinsicHeight(
          child: Row(
            children: <Widget>[
              Container(
                width: adaptiveWidth(context, 50),
                height: double.infinity,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: index == 0 ? SizedBox() : Container(
                        width: adaptiveWidth(context, 1),
                        height: double.infinity,
                        color: ColorUtils.primary.withOpacity(0.5),
                      ),
                    ),
                    Container(
                      height: adaptiveWidth(context, 10),
                      width: adaptiveWidth(context, 10),
                      decoration: BoxDecoration(
                        color: ColorUtils.primary,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Expanded(
                      child: index == itemCount - 1 ? SizedBox() : Container(
                        width: adaptiveWidth(context, 1),
                        height: double.infinity,
                        color: ColorUtils.primary.withOpacity(0.5),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: adaptiveWidth(context, 15)),
                    StyledText("${data.mapelNama} - ${Kelas.romawi(data.kelas)}",
                      size: sp(context, 14),
                      fontWeight: FontWeight.bold,
                    ),
                    StyledText("${data.materi}",
                      size: sp(context, 13),
                      fontWeight: FontWeight.w600,
                    ),
                    SizedBox(height: adaptiveWidth(context, 5)),
                    StyledText("${DateFormat("EEEE, dd MMM").format(DateFormat("yyyy-MM-dd").parse(data.tanggal))}, "
                        "${data.jamMulai} - ${data.jamSelesai}",
                      color: ColorUtils.grey7e7e,
                    ),
                    SizedBox(height: adaptiveWidth(context, 15)),
                  ],
                ),
              ),
              SizedBox(width: adaptiveWidth(context, 10)),
              Icon(Icons.more_horiz),
              SizedBox(width: adaptiveWidth(context, 20)),
            ],
          ),
        ),
      ),
    );
  }
}

class ShimmerJadwalMengajar extends StatelessWidget {
  final int index;
  final int itemCount;

  ShimmerJadwalMengajar({@required this.index, @required this.itemCount});

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        children: <Widget>[
          Container(
            width: adaptiveWidth(context, 50),
            height: double.infinity,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: index == 0 ? SizedBox() : Container(
                    width: adaptiveWidth(context, 1),
                    height: double.infinity,
                    color: ColorUtils.primary.withOpacity(0.5),
                  ),
                ),
                Container(
                  height: adaptiveWidth(context, 10),
                  width: adaptiveWidth(context, 10),
                  decoration: BoxDecoration(
                    color: ColorUtils.primary,
                    shape: BoxShape.circle,
                  ),
                ),
                Expanded(
                  child: index == itemCount - 1 ? SizedBox() : Container(
                    width: adaptiveWidth(context, 1),
                    height: double.infinity,
                    color: ColorUtils.primary.withOpacity(0.5),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: <Widget>[
                SizedBox(height: adaptiveWidth(context, 5)),
                Shimmer.fromColors(
                  child: Container(
                    height: adaptiveWidth(context, 80),
                    color: Colors.grey[200],
                  ),
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.white,
                ),
                SizedBox(height: adaptiveWidth(context, 5)),
              ],
            ),
          ),
          SizedBox(width: adaptiveWidth(context, 20)),
        ],
      ),
    );
  }
}

