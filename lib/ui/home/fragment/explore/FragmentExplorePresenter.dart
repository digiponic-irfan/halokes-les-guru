import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/network/response/general/JadwalMengajarResponse.dart';
import 'package:halokes_les_guru/ui/home/fragment/explore/FragmentExploreDelegate.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FragmentExplorePresenter extends BasePresenter {
  static const REQUEST_GET_MAPEL = 0;
  static const REQUEST_GET_JADWAL = 1;

  final FragmentExploreDelegate _delegate;
  FragmentExplorePresenter(BaseState state, this._delegate) : super(state);

  void executeGetMapel(){
    generalRepo.executeGetMapel(REQUEST_GET_MAPEL, _delegate.onSuccessGetMapel);
  }

  void executeGetJadwal(RequestWrapper<JadwalMengajarResponse> wrapper){
    wrapper.doRequest();
    generalRepo.executeGetJadwal(REQUEST_GET_JADWAL, wrapper.finishRequest);
  }
}