import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/database/database.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/network/response/general/ChatUserResponse.dart';
import 'package:halokes_les_guru/ui/home/fragment/chat/FragmentChatPresenter.dart';
import 'package:halokes_les_guru/ui/home/fragment/chat/adapter/FragmentChatAdapter.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';

class FragmentChat extends StatefulWidget {
  @override
  _FragmentChatState createState() => _FragmentChatState();
}

class _FragmentChatState extends BaseState<FragmentChat> {
  FragmentChatPresenter _presenter;

  @override
  void initState() {
    super.initState();
    _presenter = FragmentChatPresenter(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () => navigateTo(MyApp.ROUTE_CHAT_SEARCH),
        child: Icon(Icons.chat, color: Colors.white),
      ),
      appBar: AppBar(
        centerTitle: false,
        title: StyledText("Kontak", color: Colors.white),
      ),
      body: StreamBuilder<List<ListContactResult>>(
        stream: _presenter.contactStream,
        builder: (_, snapshot){
          if(snapshot.hasData){
            if(snapshot.data.length > 0){
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (_, index) => FragmentChatItem(
                  contact: snapshot.data[index],
                  onSelected: _onContactSelected,
                ),
              );
            }
          }

          return Center(
            child: Center(
              child: StyledText("Tidak ada riwayat percakapan",
                fontWeight: FontWeight.w600,
                size: sp(16),
              ),
            ),
          );
        },
      ),
    );
  }

  void _onContactSelected(ListContactResult contact) =>
      navigateTo(MyApp.ROUTE_CHAT_CONVERSATION,
        arguments: ChatUser.fromListContact(contact),
      );
}
