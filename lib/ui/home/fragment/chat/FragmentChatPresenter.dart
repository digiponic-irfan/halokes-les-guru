import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/database/database.dart';

class FragmentChatPresenter extends BasePresenter {

  Stream<List<ListContactResult>> contactStream;

  FragmentChatPresenter(BaseState state) : super(state){
    contactStream = database.watchListContact();
  }
}