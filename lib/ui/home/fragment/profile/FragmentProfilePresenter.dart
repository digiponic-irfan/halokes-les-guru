import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/network/response/general/LoginResponse.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';

class FragmentProfilePresenter extends BasePresenter {
  FragmentProfilePresenter(BaseState state) : super(state);

  void getCurrentUser(RequestWrapper<UserData> wrapper) async =>
      wrapper..doRequest()..finishRequest(await AppPreference.getUser());
}