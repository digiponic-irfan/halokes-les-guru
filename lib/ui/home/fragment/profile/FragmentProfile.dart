import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/network/response/general/LoginResponse.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';
import 'package:halokes_les_guru/ui/home/fragment/profile/FragmentProfilePresenter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:halokes_les_guru/utils/ImageUtils.dart';
import 'package:mcnmr_request_wrapper/RequestWrapper.dart';
import 'package:mcnmr_request_wrapper/RequestWrapperWidget.dart';

class FragmentProfile extends StatefulWidget {
  @override
  _FragmentProfileState createState() => _FragmentProfileState();
}

class _FragmentProfileState extends BaseState<FragmentProfile> {
  FragmentProfilePresenter _presenter;
  RequestWrapper<UserData> _userWrapper = RequestWrapper();

  @override
  void initState() {
    super.initState();
    _presenter = FragmentProfilePresenter(this);
  }

  @override
  void afterWidgetBuilt() {
    _presenter.getCurrentUser(_userWrapper);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.greyf1f1,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Image.asset(ImageUtils.img_profile,
                  width: double.infinity,
                  height: width(180),
                  fit: BoxFit.cover,
                ),
                Container(
                  margin: EdgeInsets.only(top: width(150)),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(width(20)),
                      topLeft: Radius.circular(width(20)),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Spacer(),
                          MaterialButton(
                            onPressed: () => navigateTo(MyApp.ROUTE_UBAH_PROFILE),
                            elevation: 0,
                            color: Colors.white,
                            height: 35,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(width(20)),
                              side: BorderSide(color: ColorUtils.primary),
                            ),
                            child: StyledText("Edit Profile",
                              color: ColorUtils.primary,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(width: width(20)),
                        ],
                      ),
                      SizedBox(height: width(20)),
                      Padding(
                        padding: EdgeInsets.only(left: width(30)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            RequestWrapperWidget<UserData>(
                              requestWrapper: _userWrapper,
                              placeholder: (_) => StyledText("",
                                size: sp(14),
                                fontWeight: FontWeight.w600,
                              ),
                              builder: (_, data) => StyledText(data.guruNama,
                                size: sp(14),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(height: width(10)),
                            RequestWrapperWidget<UserData>(
                              requestWrapper: _userWrapper,
                              placeholder: (_) => StyledText("",
                                size: sp(12),
                              ),
                              builder: (_, data) => StyledText("Guru di ${data.guruAsalSekolah}",
                                size: sp(12),
                              ),
                            ),
                            SizedBox(height: width(20)),
                            StyledText("Email",
                              color: ColorUtils.grey7e7e,
                              size: sp(12),
                            ),
                            RequestWrapperWidget<UserData>(
                              requestWrapper: _userWrapper,
                              placeholder: (_) => StyledText("",
                                size: sp(12),
                              ),
                              builder: (_, data) => StyledText(data.email,
                                size: sp(12),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: width(20)),
                      Container(
                        color: ColorUtils.greyf1f1,
                        height: width(10),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: width(10)),
                          Padding(
                            padding: EdgeInsets.only(left: width(30)),
                            child: StyledText("Keamanan",
                              size: sp(14),
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(height: width(5)),
                          Material(
                            color: Colors.white,
                            child: InkWell(
                              onTap: () => navigateTo(MyApp.ROUTE_UBAH_PASSWORD),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                  horizontal: width(30),
                                  vertical: width(15),
                                ),
                                child: Row(
                                  children: <Widget>[
                                    StyledText("Ubah Sandi",
                                      size: sp(12),
                                    ),
                                    Spacer(),
                                    Icon(Icons.arrow_forward_ios, color: Colors.black),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: width(5)),
                        ],
                      ),
                      Container(
                        color: ColorUtils.greyf1f1,
                        padding: EdgeInsets.symmetric(
                          vertical: width(20),
                          horizontal: width(10),
                        ),
                        child: MaterialButton(
                          onPressed: () async {
                            await AppPreference.removeUser();
                            navigateTo(MyApp.ROUTE_LOGIN, singleTop: true);
                          },
                          color: ColorUtils.greyf1f1,
                          minWidth: double.infinity,
                          height: width(40),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(width(10)),
                            side: BorderSide(color: ColorUtils.primary),
                          ),
                          child: StyledText("Logout",
                            color: ColorUtils.primary,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: width(100),
                    left: width(30),
                  ),
                  width: width(100),
                  height: width(100),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: Container(
                    width: width(90),
                    height: width(90),
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      shape: BoxShape.circle,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
