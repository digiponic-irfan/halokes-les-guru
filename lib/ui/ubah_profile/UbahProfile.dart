import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';
import 'package:halokes_les_guru/ui/ubah_profile/UbahProfileDelegate.dart';
import 'package:halokes_les_guru/ui/ubah_profile/UbahProfilePresenter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';

class UbahProfile extends StatefulWidget {
  @override
  _UbahProfileState createState() => _UbahProfileState();
}

class _UbahProfileState extends BaseState<UbahProfile> with UbahProfileDelegate{
  UbahProfilePresenter _presenter;

  TextEditingController _namaController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _asalSekolahController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _presenter = UbahProfilePresenter(this, this);
  }

  @override
  void afterWidgetBuilt() async {
    var user = await AppPreference.getUser();
    _namaController.text = user.guruNama;
    _emailController.text = user.email;
    _asalSekolahController.text = user.guruAsalSekolah;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.greyf1f1,
      appBar: AppBar(
        title: StyledText("Ubah Profile", color: Colors.white),
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: finish, color: Colors.white),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(20)),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(width(15)),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(width(10)),
              ),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    style: StyledText.style(
                      size: sp(14),
                    ),
                    controller: _namaController,
                    decoration: InputDecoration(
                      labelText: "Nama",
                      labelStyle: StyledText.style(
                        color: ColorUtils.grey7e7e,
                        size: sp(14),
                      ),
                    ),
                  ),
                  SizedBox(height: width(10)),
                  TextFormField(
                    style: StyledText.style(
                      size: sp(14),
                    ),
                    controller: _emailController,
                    decoration: InputDecoration(
                      labelText: "Email",
                      labelStyle: StyledText.style(
                        color: ColorUtils.grey7e7e,
                        size: sp(14),
                      ),
                    ),
                  ),
                  SizedBox(height: width(10)),
                  TextFormField(
                    style: StyledText.style(
                      size: sp(14),
                    ),
                    controller: _asalSekolahController,
                    decoration: InputDecoration(
                      labelText: "Asal Sekolah",
                      labelStyle: StyledText.style(
                        color: ColorUtils.grey7e7e,
                        size: sp(14),
                      ),
                    ),
                  ),
                  SizedBox(height: width(10)),
                ],
              ),
            )
          ],
        ),
      )
    );
  }
}
