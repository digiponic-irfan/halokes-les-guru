import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/ui/ubah_profile/UbahProfileDelegate.dart';

class UbahProfilePresenter extends BasePresenter {

  final UbahProfileDelegate _delegate;
  UbahProfilePresenter(BaseState state, this._delegate) : super(state);

}