import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:halokes_les_guru/ancestor/BaseResponse.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/network/response/general/MapelResponse.dart';
import 'package:halokes_les_guru/ui/buat_pertemuan/BuatPertemuanArgument.dart';
import 'package:halokes_les_guru/ui/jadwal_mengajar/buat/BuatJadwalMengajarDelegate.dart';
import 'package:halokes_les_guru/ui/jadwal_mengajar/buat/BuatJadwalMengajarPresenter.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:numberpicker/numberpicker.dart';

class BuatJadwalMengajar extends StatefulWidget {
  final MapelResponse argument;

  BuatJadwalMengajar({@required this.argument}) : assert(argument != null);

  @override
  _BuatJadwalMengajarState createState() => _BuatJadwalMengajarState();
}

class _BuatJadwalMengajarState extends BaseState<BuatJadwalMengajar> with BuatJadwalMengajarDelegate{
  BuatJadwalMengajarPresenter _presenter;

  MapelData _selectedMapel;
  String _kelas = "";
  int _jumlahPertemuan = 0;
  String _harga = "";

  FocusNode _kelasFocus = FocusNode();
  FocusNode _hargaFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    _presenter = BuatJadwalMengajarPresenter(this, this);
  }

  @override
  void onValid(BuatJadwalParams params) {
    navigateTo(MyApp.ROUTE_BUAT_PERTEMUAN,
      arguments: BuatPertemuanArgument(
        pertemuanKe: 1,
        jumlahPertemuan: _jumlahPertemuan,
        pertemuanParams: List<BuatPertemuanParams>(),
        jadwalParams: params,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: finish),
        title: StyledText("Buat Jadwal Mengajar",
          color: Colors.white,
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(20)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            StyledText("Mata Pelajaran",
              size: sp(14),
            ),
            SizedBox(height: width(10)),
            Container(
              width: double.infinity,
              height: width(40),
              padding: EdgeInsets.symmetric(horizontal: width(10)),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(width(10)),
                border: Border.all(color: ColorUtils.primary),
              ),
              child: DropdownButton<MapelData>(
                items: widget.argument.data.map((it) => DropdownMenuItem<MapelData>(
                  value: it,
                  child: StyledText(it.mapelNama, size: sp(12)),
                )).toList(),
                onChanged: (it) => setState(() => _selectedMapel = it),
                isExpanded: true,
                value: _selectedMapel,
                iconEnabledColor: Colors.black,
                iconDisabledColor: Colors.black,
                underline: SizedBox(),
                hint: StyledText("Contoh: Matematika",
                  color: ColorUtils.grey3333.withOpacity(0.4),
                  size: sp(12),
                ),
              ),
            ),

            SizedBox(height: width(25)),
            StyledText("Jumlah Pertemuan",
              size: sp(14),
            ),
            SizedBox(height: width(10)),

            Container(
              decoration: BoxDecoration(
                color: ColorUtils.primary,
                borderRadius: BorderRadius.circular(width(10)),
              ),
              child: Theme(
                data: ThemeData(
                  textTheme: TextTheme(
                    body1: StyledText.style(
                      size: width(18),
                      color: Colors.white.withOpacity(0.75),
                    ),
                    headline: StyledText.style(
                      size: width(24),
                    ),
                  ),
                  accentColor: Colors.white,
                ),
                child: NumberPicker.integer(
                  initialValue: _jumlahPertemuan,
                  minValue: 0,
                  maxValue: 99,
                  onChanged: (it) => setState(() => _jumlahPertemuan = it),
                ),
              ),
            ),

            SizedBox(height: width(25)),

            StyledText("Kelas",
              size: sp(14),
            ),
            SizedBox(height: width(10)),
            Container(
              width: double.infinity,
              height: width(40),
              padding: EdgeInsets.symmetric(
                horizontal: width(10),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(width(10)),
                border: Border.all(color: ColorUtils.primary),
              ),
              child: TextFormField(
                onChanged: (it) => _kelas = it,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.characters,
                focusNode: _kelasFocus,
                onFieldSubmitted: (_){
                  _kelasFocus.unfocus();
                  _hargaFocus.requestFocus();
                },
                expands: true,
                maxLines: null,
                minLines: null,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  hintText: "Contoh: VII",
                  hintStyle: StyledText.style(
                    color: ColorUtils.grey3333.withOpacity(0.4),
                    size: sp(14),
                  ),
                  border: InputBorder.none,
                ),
              ),
            ),

            SizedBox(height: width(25)),

            StyledText("Harga per pertemuan",
              size: sp(14),
            ),
            SizedBox(height: width(10)),
            Container(
              width: double.infinity,
              height: width(40),
              padding: EdgeInsets.symmetric(
                horizontal: width(10),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(width(10)),
                border: Border.all(color: ColorUtils.primary),
              ),
              child: TextFormField(
                onChanged: (it) => _harga = it,
                keyboardType: TextInputType.number,
                textCapitalization: TextCapitalization.words,
                focusNode: _hargaFocus,
                onFieldSubmitted: (_) => _hargaFocus.unfocus(),
                expands: true,
                maxLines: null,
                minLines: null,
                textInputAction: TextInputAction.done,
                decoration: InputDecoration(
                  hintText: "Contoh: 10000",
                  hintStyle: StyledText.style(
                    color: ColorUtils.grey3333.withOpacity(0.4),
                    size: sp(14),
                  ),
                  border: InputBorder.none,
                ),
              ),
            ),

            SizedBox(height: width(25)),

            MaterialButton(
              onPressed: () => _presenter.validate(_selectedMapel, _kelas, _jumlahPertemuan, _harga),
              color: ColorUtils.primary,
              minWidth: double.infinity,
              height: width(40),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(width(10)),
              ),
              child: StyledText("Lanjut",
                color: Colors.white,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
      ),
    );
  }

}
