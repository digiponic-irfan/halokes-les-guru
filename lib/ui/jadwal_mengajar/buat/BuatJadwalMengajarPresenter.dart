import 'package:halokes_les_guru/ancestor/BasePresenter.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/extension/Kelas.dart';
import 'package:halokes_les_guru/network/response/general/MapelResponse.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';
import 'package:halokes_les_guru/ui/buat_pertemuan/BuatPertemuanArgument.dart';
import 'package:halokes_les_guru/ui/jadwal_mengajar/buat/BuatJadwalMengajarDelegate.dart';
import 'package:intl/intl.dart';

class BuatJadwalMengajarPresenter extends BasePresenter {
  static const REQUEST_BUAT_JADWAL = 0;

  final BuatJadwalMengajarDelegate _delegate;
  BuatJadwalMengajarPresenter(BaseState state, this._delegate) : super(state);

  void executeBuatJadwal(MapelData selectedMapel, String kelas, String materi, String harga, DateTime selectedDate,
      int jamMulai, int menitMulai, int jamAkhir, int menitAkhir) async {

    if(selectedMapel == null){
      state.alert(title: "Error", message: "Mata pelajaran belum dipilih");
      return;
    }

    if(kelas == ""){
      state.alert(title: "Error", message: "kelas belum diisi");
      return;
    }

    if(materi == ""){
      state.alert(title: "Error", message: "Materi belum diisi");
      return;
    }

    if(harga == ""){
      state.alert(title: "Error", message: "Harga belum diisi");
      return;
    }

    if(selectedDate == null){
      state.alert(title: "Error", message: "Tanggal belum dipilih");
      return;
    }

    var start = (jamMulai * 60) + menitMulai;
    var end = (jamAkhir * 60) + menitAkhir;

    if(start > end){
      state.alert(title: "Error", message: "Jam mulai tidak bisa lebih besar daripada jam berakhir");
      return;
    }

    if(start == end){
      state.alert(title: "Error", message: "Jam mulai tidak bisa sama dengan jam berakhir");
      return;
    }

    if((start - end) < 30){
      state.alert(title: "Error", message: "Sesi minimal berlangsung selama 30 menit");
      return;
    }

    var user = await AppPreference.getUser();

    var params = {
      "id_guru" : user.idGuru,
      "id_mapel" : selectedMapel.idMapelUrl,
      "materi" : materi,
      "kelas" : Kelas.angka(kelas),
      "jumlah" : "1",
      "harga" : harga,
      "tanggal" : DateFormat("yyyy-MM-dd").format(selectedDate),
      "jam_mulai" : "$jamMulai:$menitMulai:00",
      "jam_selesai" : "$jamAkhir:$menitAkhir:00"
    };

    //generalRepo.executeBuatJadwal(REQUEST_BUAT_JADWAL, params, _delegate.onSuccessBuatJadwal);
  }

  void validate(MapelData selectedMapel, String kelas, int jumlahPertemuan, String harga){
    if(selectedMapel == null){
      state.alert(title: "Error", message: "Mata pelajaran belum dipilih", positiveTitle: "Tutup");
      return;
    }

    if(kelas == ""){
      state.alert(title: "Error", message: "kelas belum diisi", positiveTitle: "Tutup");
      return;
    }

    if(harga == ""){
      state.alert(title: "Error", message: "Harga belum diisi", positiveTitle: "Tutup");
      return;
    }

    if(jumlahPertemuan <= 0){
      state.alert(title: "Error", message: "Minial 1 pertemuan");
      return;
    }

    _delegate.onValid(BuatJadwalParams(mapel: selectedMapel, kelas: kelas, harga: harga));
  }
}