import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/BaseStateExt.dart';
import 'package:halokes_les_guru/extension/Kelas.dart';
import 'package:halokes_les_guru/network/response/general/JadwalMengajarResponse.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:halokes_les_guru/utils/ImageUtils.dart';
import 'package:intl/intl.dart';

class DetailJadwalMengajar extends StatefulWidget {
  final JadwalMengajarData argument;

  DetailJadwalMengajar({@required this.argument});

  @override
  _DetailJadwalMengajarState createState() => _DetailJadwalMengajarState();
}

class _DetailJadwalMengajarState extends BaseState<DetailJadwalMengajar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: finish),
        title: StyledText("Detail Kelas",
          color: Colors.white,
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(width(20)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            StyledText("Mata pelajaran",
              size: sp(12),
              color: ColorUtils.grey7e7e,
            ),
            StyledText(widget.argument.mapelNama,
              size: sp(14),
            ),

            SizedBox(height: width(20)),

            StyledText("Materi",
              size: sp(12),
              color: ColorUtils.grey7e7e,
            ),
            StyledText(widget.argument.materi,
              size: sp(14),
            ),

            SizedBox(height: width(20)),

            StyledText("Kelas",
              size: sp(12),
              color: ColorUtils.grey7e7e,
            ),
            StyledText("${Kelas.romawi(widget.argument.kelas)} (Kelas ${widget.argument.kelas})",
              size: sp(14),
            ),

            SizedBox(height: width(20)),

            StyledText("Peserta",
              size: sp(12),
              color: ColorUtils.grey7e7e,
            ),
            StyledText("-",
              size: sp(14),
            ),

            SizedBox(height: width(20)),

            StyledText("Tangga, waktu",
              size: sp(12),
              color: ColorUtils.grey7e7e,
            ),
            StyledText("${DateFormat("EEEE, dd MMM").format(DateFormat("yyyy-MM-dd").parse(widget.argument.tanggal))}"
                "\n${widget.argument.jamMulai} - ${widget.argument.jamSelesai}",
              size: sp(14),
            ),

            SizedBox(height: width(20)),
            
            Row(
              children: <Widget>[
                SvgPicture.asset(ImageUtils.ic_google_meet,
                  width: width(42),
                  height: width(42),
                  fit: BoxFit.fill,
                ),
                SizedBox(width: width(10)),
                StyledText("Link",
                  size: sp(14),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
