import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/ui/image_previewer/ImagePreviewerArgument.dart';
import 'package:photo_view/photo_view.dart';

class ImagePreviewer extends StatefulWidget {
  final ImagePreviewerArgument argument;

  ImagePreviewer({@required this.argument}) : assert(argument != null);

  @override
  _ImagePreviewerState createState() => _ImagePreviewerState();
}

class _ImagePreviewerState extends BaseState<ImagePreviewer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        leading: IconButton(icon: Icon(Icons.arrow_back_ios, color: Colors.white), onPressed: () => finish()),
      ),
      body: PhotoView(imageProvider: widget.argument.provider,
        minScale: PhotoViewComputedScale.contained * 1,
        maxScale: PhotoViewComputedScale.contained * 8,
      ),
    );
  }
}
