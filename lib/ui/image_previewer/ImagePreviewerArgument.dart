import 'dart:io';

import 'package:flutter/cupertino.dart';

class ImagePreviewerArgument {
  ImageProvider provider;

  ImagePreviewerArgument.fromFile(File file){
    provider = FileImage(file);
  }

  ImagePreviewerArgument.fromAsset(String assetName){
    provider = AssetImage(assetName);
  }

  ImagePreviewerArgument.fromNetwork(String url){
    provider = NetworkImage(url);
  }

  ImagePreviewerArgument.fromProvider(ImageProvider provider){
    this.provider = provider;
  }
}