import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/extension/Size.dart' as s;

///
/// Extension of BaseState class
///
extension BaseStateExt on BaseState {
  width(double size) => s.adaptiveWidth(this.context, size);

  height(double size) => s.adaptiveHeight(this.context, size);

  fullWidth() => s.adaptiveWidth(this.context, s.MAX_WIDTH);

  fullHeight({bool withToolbar = true, bool withStatusbar = true}){
    var height = s.adaptiveHeight(this.context, s.MAX_HEIGHT);

    if(!withToolbar){
      height -= kToolbarHeight;
    }

    if(!withStatusbar){
      height -= MediaQuery.of(context).padding.top;
    }

    return height;
  }

  sp(double size) => s.sp(this.context, size);
}