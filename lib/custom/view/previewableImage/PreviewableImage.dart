import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/main.dart';
import 'package:halokes_les_guru/ui/image_previewer/ImagePreviewerArgument.dart';

abstract class PreviewableImageFactory extends StatefulWidget {

  PreviewableImageFactory({Key key}) : super(key: key);

  factory PreviewableImageFactory.fromFile(File file, {double scale = 1.0, Key key,
    BorderRadiusGeometry borderRadius, BoxBorder border, ShapeBorder shapeBorder,
    BoxShape shape = BoxShape.rectangle, double width,
    double height, BoxConstraints constraints, BoxFit boxFit = BoxFit.cover}) {
    return _PreviewableImage(
      provider: FileImage(file, scale: scale),
      key: key,
      borderRadius: borderRadius,
      border: border,
      shape: shape,
      width: width,
      height: height,
      constraints: constraints,
      boxFit: boxFit,
      shapeBorder: shapeBorder,
    );
  }

  factory PreviewableImageFactory.fromAsset(String assetPath, {AssetBundle bundle, String package,
    Key key, BorderRadiusGeometry borderRadius, BoxBorder border, BoxFit boxFit = BoxFit.cover,
    BoxShape shape = BoxShape.rectangle, double width, double height, ShapeBorder shapeBorder,
    BoxConstraints constraints}){
    return _PreviewableImage(
      provider: AssetImage(assetPath, bundle: bundle, package: package),
      key: key,
      borderRadius: borderRadius,
      border: border,
      shape: shape,
      width: width,
      height: height,
      constraints: constraints,
      boxFit: boxFit,
      shapeBorder: shapeBorder,
    );
  }

  factory PreviewableImageFactory.fromImageProvider(ImageProvider provider, {Key key,
    BorderRadiusGeometry borderRadius, BoxBorder border, BoxFit boxFit = BoxFit.cover,
    BoxShape shape = BoxShape.rectangle, double width, double height, ShapeBorder shapeBorder,
    BoxConstraints constraints}){
    return _PreviewableImage(
      provider: provider,
      key: key,
      borderRadius: borderRadius,
      border: border,
      shape: shape,
      width: width,
      height: height,
      constraints: constraints,
      boxFit: boxFit,
      shapeBorder: shapeBorder,
    );
  }
}

class _PreviewableImage extends PreviewableImageFactory {
  final Key key;
  final BorderRadiusGeometry borderRadius;
  final BoxBorder border;
  final BoxShape shape;
  final double width;
  final double height;
  final BoxConstraints constraints;
  final ImageProvider provider;
  final BoxFit boxFit;
  final ShapeBorder shapeBorder;

  _PreviewableImage({this.key, this.provider, this.borderRadius, this.border,
    this.shape = BoxShape.rectangle, this.width, this.height, this.shapeBorder,
    this.constraints, this.boxFit}) : super(key: key);

  @override
  _PreviewableFileImageState createState() => _PreviewableFileImageState();
}

class _PreviewableFileImageState extends BaseState<_PreviewableImage> {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            borderRadius: widget.borderRadius,
            border: widget.border,
            shape: widget.shape,
            image: DecorationImage(
              image: widget.provider,
              fit: widget.boxFit,
            ),
          ),
        ),
        Positioned.fill(
          child: Material(
            type: MaterialType.transparency,
            shape: widget.shapeBorder,
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              onTap: () => navigateTo(MyApp.ROUTE_IMAGE_PREVIEWER,
                arguments: ImagePreviewerArgument.fromProvider(widget.provider),
              ),
            ),
          ),
        )
      ],
    );
  }
}

