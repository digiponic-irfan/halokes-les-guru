import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/Size.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';

class PhotoOptionsSheetDialog extends StatelessWidget {
  static const RESULT_CAMERA = 0;
  static const RESULT_GALLERY = 1;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: adaptiveWidth(context, 10),
        horizontal: adaptiveWidth(context, 20),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          StyledText("Foto Profil",
            size: sp(context, 16),
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: adaptiveWidth(context, 30)),
          Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Material(
                    color: ColorUtils.primary,
                    clipBehavior: Clip.antiAlias,
                    shape: CircleBorder(),
                    child: InkWell(
                      onTap: () => Navigator.of(context).pop(RESULT_CAMERA),
                      child: Container(
                        width: adaptiveWidth(context, 60),
                        height: adaptiveWidth(context, 60),
                        alignment: Alignment.center,
                        child: Icon(Icons.camera_alt, color: Colors.white, size: adaptiveWidth(context, 30)),
                      ),
                    ),
                  ),
                  SizedBox(height: adaptiveWidth(context, 10)),
                  StyledText("Kamera",
                    size: sp(context, 14),
                  )
                ],
              ),
              SizedBox(width: adaptiveWidth(context, 20)),
              Column(
                children: <Widget>[
                  Material(
                    color: ColorUtils.primary,
                    clipBehavior: Clip.antiAlias,
                    shape: CircleBorder(),
                    child: InkWell(
                      onTap: () => Navigator.of(context).pop(RESULT_GALLERY),
                      child: Container(
                        width: adaptiveWidth(context, 60),
                        height: adaptiveWidth(context, 60),
                        alignment: Alignment.center,
                        child: Icon(Icons.image, color: Colors.white, size: adaptiveWidth(context, 30)),
                      ),
                    ),
                  ),
                  SizedBox(height: adaptiveWidth(context, 10)),
                  StyledText("Galeri",
                    size: sp(context, 14),
                  )
                ],
              ),
            ],
          ),
          SizedBox(height: adaptiveWidth(context, 50)),
        ],
      ),
    );
  }
}
