import 'package:flutter/material.dart';
import 'package:halokes_les_guru/custom/view/text/StyledText.dart';
import 'package:halokes_les_guru/extension/Size.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';

class AndroidOptionDialog extends StatelessWidget {
  final String message;
  final String negativeTitle;
  final String positiveTitle;
  final VoidCallback onNegative;
  final VoidCallback onPositive;
  final bool dismissible;

  AndroidOptionDialog({this.message, this.negativeTitle, this.positiveTitle,
    this.onNegative, this.onPositive, this.dismissible = true});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Dialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(adaptiveWidth(context, 30))
        ),
        child: Padding(
          padding: EdgeInsets.all(adaptiveWidth(context, 20)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: adaptiveWidth(context, 10)),
              StyledText(message,
                color: ColorUtils.primary,
                size: sp(context, 14),
              ),
              SizedBox(height: adaptiveWidth(context, 20)),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  MaterialButton(
                    onPressed: onNegative ?? () => Navigator.of(context).pop(),
                    color: Colors.white,
                    minWidth: adaptiveWidth(context, 100),
                    height: adaptiveWidth(context, 30),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(adaptiveWidth(context, 30)),
                      side: BorderSide(color: ColorUtils.warning),
                    ),
                    child: StyledText(negativeTitle,
                      color: ColorUtils.primary,
                      size: sp(context, 14),
                    ),
                  ),
                  SizedBox(width: adaptiveWidth(context, 30)),
                  MaterialButton(
                    onPressed: onPositive ?? () => Navigator.of(context).pop(),
                    color: ColorUtils.primary,
                    minWidth: adaptiveWidth(context, 100),
                    height: adaptiveWidth(context, 30),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(adaptiveWidth(context, 30))
                    ),
                    child: StyledText(positiveTitle,
                      color: Colors.white,
                      size: sp(context, 14),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      onWillPop:() async => dismissible,
    );
  }
}
