import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/provider/ChatLastSeenProvider.dart';
import 'package:halokes_les_guru/provider/TypingProvider.dart';
import 'package:halokes_les_guru/ui/buat_pertemuan/BuatPertemuan.dart';
import 'package:halokes_les_guru/ui/chat/conversation/ChatConversation.dart';
import 'package:halokes_les_guru/ui/chat/search/ChatSearch.dart';
import 'package:halokes_les_guru/ui/home/Home.dart';
import 'package:halokes_les_guru/ui/image_previewer/ImagePreviewer.dart';
import 'package:halokes_les_guru/ui/jadwal_mengajar/buat/BuatJadwalMengajar.dart';
import 'package:halokes_les_guru/ui/jadwal_mengajar/detail/DetailJadwalMengajar.dart';
import 'package:halokes_les_guru/ui/login/Login.dart';
import 'package:halokes_les_guru/ui/pertemuan/success/SuccessBuatPertemuan.dart';
import 'package:halokes_les_guru/ui/register/Register.dart';
import 'package:halokes_les_guru/ui/splash_screen/SplashScreen.dart';
import 'package:halokes_les_guru/ui/ubah_password/UbahPassword.dart';
import 'package:halokes_les_guru/ui/ubah_profile/UbahProfile.dart';
import 'package:halokes_les_guru/utils/ColorUtils.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const ROUTE_LOGIN = "/login";
  static const ROUTE_REGISTER = "/register";
  static const ROUTE_HOME = "/home";
  static const ROUTE_SPLASH_SCREEN = "/splashScreen";
  static const ROUTE_BUAT_JADWAL_MENGAJAR = "/buatJadwalMengajar";
  static const ROUTE_BUAT_PERTEMUAN = "/buatPertemuan";
  static const ROUTE_DETAIL_JADWAL = "/detailJadwal";
  static const ROUTE_CHAT_CONVERSATION = "/chatConversation";
  static const ROUTE_CHAT_SEARCH = "/chatSearch";
  static const ROUTE_UBAH_PROFILE = "/ubahProfile";
  static const ROUTE_UBAH_PASSWORD = "/ubahPassword";
  static const ROUTE_SUCCESS_BUAT_PERTEMUAN = "/successBuatPertemuan";

  static const ROUTE_IMAGE_PREVIEWER = "/imagePreviewer";

  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ChatLastSeenProvider>(create: (_) => ChatLastSeenProvider()),
        ChangeNotifierProvider<TypingProvider>(create: (_) => TypingProvider()),
      ],
      child: MaterialApp(
        title: 'Halokes Les Guru',
        theme: ThemeData(
          primaryColor: ColorUtils.primary,
        ),
        debugShowCheckedModeBanner: false,
        initialRoute: ROUTE_SPLASH_SCREEN,
        onGenerateRoute: (settings){
          switch(settings.name){
            case ROUTE_SPLASH_SCREEN :
              return _createRoute(target: SplashScreen());
            case ROUTE_LOGIN :
              return _createRoute(target: Login());
            case ROUTE_CHAT_CONVERSATION :
              return _createRoute(target: ChatConversation(argument: settings.arguments));
            case ROUTE_CHAT_SEARCH :
              return _createRoute(target: ChatSearch());
            case ROUTE_REGISTER :
              return _createRoute(target: Register());
            case ROUTE_HOME :
              return _createRoute(target: Home());
            case ROUTE_UBAH_PROFILE :
              return _createRoute(target: UbahProfile());
            case ROUTE_UBAH_PASSWORD :
              return _createRoute(target: UbahPassword());
            case ROUTE_BUAT_JADWAL_MENGAJAR :
              return _createRoute(target: BuatJadwalMengajar(argument: settings.arguments));
            case ROUTE_BUAT_PERTEMUAN :
              return _createRoute(target: BuatPertemuan(argument: settings.arguments));
            case ROUTE_SUCCESS_BUAT_PERTEMUAN :
              return _createRoute(target: SuccessBuatPertemuan());
            case ROUTE_DETAIL_JADWAL :
              return _createRoute(target: DetailJadwalMengajar(argument: settings.arguments));
            case ROUTE_IMAGE_PREVIEWER :
              return _createRoute(target: ImagePreviewer(argument: settings.arguments));
            default:
              return _createRoute(target: SplashScreen());
          }
        },
      ),
    );
  }

  ///
  /// Create route depends by device platform
  ///
  PageRoute _createRoute({@required Widget target}){
    if(Platform.isIOS){
      return CupertinoPageRoute(builder: (_) => target);
    }else {
      return MaterialPageRoute(builder: (_) => target);
    }
  }
}
