class ImageUtils {
  static const ic_google_meet = "images/ic_google_meet.svg";
  static const img_profile = "images/img_profile.png";
  static const image_not_found = "images/image_not_found.png";
  static const vector_success_buat_jadwal = "images/vector_success_buat_jadwal.svg";

  static const ic_pending = "images/ic_pending.svg";
  static const ic_received_by_server = "images/ic_received_by_server.svg";
  static const ic_sent = "images/ic_sent.svg";
  static const ic_read = "images/ic_read.svg";
}