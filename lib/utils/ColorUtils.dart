import 'package:flutter/material.dart';

class ColorUtils{
  static const primary = Color(0xff007bc1);

  static const grey4e4e = Color(0xff4e4e4e);
  static const greyf1f1 = Color(0xfff1f1f1);
  static const greydede = Color(0xffdedede);
  static const grey7070 = Color(0xff707070);
  static const grey7e7e = Color(0xff7e7e7e);
  static const greye7e7 = Color(0xffe7e7e7);
  static const grey3333 = Color(0xff333333);

  static const whitef6f8 = Color(0xffF5F6F8);

  static const warning = Color(0xfffca424);
}