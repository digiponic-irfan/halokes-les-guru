import 'package:halokes_les_guru/database/entities/Conversation.dart';
import 'package:halokes_les_guru/database/entities/Interlocutor.dart';
import 'package:halokes_les_guru/network/response/general/ChatUserResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/AskDelayedMessageResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/AskLatestMessageStatusResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/ServerReceivedYourMessageResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/SomeoneSendMessage.dart';
import 'package:halokes_les_guru/network/response/socket_chat/TargetReadYourMessageResponse.dart';
import 'package:halokes_les_guru/network/response/socket_chat/TargetReceivedYourMessageResponse.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'database.g.dart';

@UseMoor(
  tables: [InterlocutorEntity, ConversationEntity],
  queries: {
    "listContact" : "SELECT i.id_url, i.username, i.name, i.profile_picture, i.last_chat_time, "
        "i.display_last_chat, c.conversation_status, c.sender, "
        "(SELECT COUNT(*) FROM conversation cv WHERE cv.interlocutor_id = i.id_url AND cv.sender = -2 AND cv.conversation_status != 3) AS unread_count "
        "FROM interlocutor i "
        "LEFT JOIN conversation c ON i.last_chat_id = c.conversation_id "
        "ORDER BY i.last_chat_time DESC"
  }
)
class MyDatabase extends _$MyDatabase {
  static MyDatabase _instance;

  MyDatabase() : super(FlutterQueryExecutor.inDatabaseFolder(path: "db.sqlite"));

  static MyDatabase instance(){
    if(_instance == null){
      _instance = MyDatabase();
    }
    
    return _instance;
  }

  @override
  int get schemaVersion => 1;


  @override
  MigrationStrategy get migration => MigrationStrategy(
      onCreate: (Migrator m) {
        return m.createAll();
      },
  );

  Future<void> clearChat() async {
    await delete(interlocutorEntity).go();
    await delete(conversationEntity).go();
  }

  Stream<List<Conversation>> watchConversation(String idUrl){
    return (select(conversationEntity)..orderBy([
      (it) => OrderingTerm(expression: it.sendAt, mode: OrderingMode.desc),
    ])..where((it) => it.interlocutorId.equals(idUrl))).watch();
  }

  Future<List<Interlocutor>> getAllContact(){
    return (select(interlocutorEntity)..orderBy([
          (it) => OrderingTerm(expression: it.lastChatTime, mode: OrderingMode.desc)
    ])).get();
  }

  Future<List<Conversation>> undeliveredMessage(){
    return (select(conversationEntity)..orderBy([
          (it) => OrderingTerm(expression: it.sendAt, mode: OrderingMode.asc),
    ])..where((it) => it.conversationStatus.equals(ConversationEntity.STATUS_SEND))).get();
  }

  Future<List<Conversation>> unreadUnsentMessage(){
    return (select(conversationEntity)..orderBy([
          (it) => OrderingTerm(expression: it.sendAt, mode: OrderingMode.asc),
    ])..where((it) => it.conversationStatus.equals(ConversationEntity.STATUS_RECEIVED) |
                      it.conversationStatus.equals(ConversationEntity.STATUS_SENT),
    )..where((it) => it.sender.equals(ConversationEntity.ME))).get();
  }

  Future<void> receiveMessage(SomeoneSendMessageResponse data) async {
    var isContactExists = (await (select(interlocutorEntity)..where((it) => it.idUrl.equals(data.senderUrlId)))
        .getSingle()) != null;

    if(isContactExists){
      await (update(interlocutorEntity)..where((it) => it.idUrl.equals(data.senderUrlId))).write(
        InterlocutorEntityCompanion(
          lastChatId: Value(data.conversationId),
          displayLastChat: Value(data.conversation),
          lastChatTime: Value(data.receivedByServerAt.toDouble()),
        ),
      );
    }else {
      await into(interlocutorEntity).insert(InterlocutorEntityCompanion(
        idUrl: Value(data.senderUrlId),
        username: Value(data.senderUsername),
        name: Value(data.senderName),
        profilePicture: Value(data.senderFoto),
        lastChatId: Value(data.conversationId),
        displayLastChat: Value(data.conversation),
        lastChatTime: Value(data.receivedByServerAt.toDouble()),
      ));
    }

    await into(conversationEntity).insert(ConversationEntityCompanion(
      interlocutorId: Value(data.senderUrlId),
      conversationId: Value(data.conversationId),
      conversation: Value(data.conversation),
      conversationStatus: Value(ConversationEntity.STATUS_SENT),
      sender: Value(ConversationEntity.THEM),
      sendAt: Value(data.receivedByServerAt.toDouble()),
      receivedByServerAt: Value(data.receivedByServerAt.toDouble()),
      sentAt: Value(data.receivedByServerAt.toDouble()),
    ));
  }

  Future<void> receiveDelayedMessage(AskDelayedMessageResponse data) async {
    await Future.forEach<AskDelayedMessageData>(data.data, (loop) async {
      var isContactExists = (await (select(interlocutorEntity)..where((it) => it.idUrl.equals(loop.senderUrlId)))
          .getSingle()) != null;

      if(isContactExists){
        await (update(interlocutorEntity)..where((it) => it.idUrl.equals(loop.senderUrlId))).write(
          InterlocutorEntityCompanion(
            lastChatId: Value(loop.idConversation),
            displayLastChat: Value(loop.conversation),
            lastChatTime: Value(loop.receivedByTargetAt.toDouble()),
          ),
        );
      }else {
        await into(interlocutorEntity).insert(InterlocutorEntityCompanion(
          idUrl: Value(loop.senderUrlId),
          username: Value(loop.sender.username),
          name: Value(loop.sender.name),
          profilePicture: Value(loop.sender.foto),
          lastChatId: Value(loop.idConversation),
          displayLastChat: Value(loop.conversation),
          lastChatTime: Value(loop.receivedByTargetAt.toDouble()),
        ));
      }

      await into(conversationEntity).insert(ConversationEntityCompanion(
        interlocutorId: Value(loop.senderUrlId),
        conversationId: Value(loop.idConversation),
        conversation: Value(loop.conversation),
        conversationStatus: Value(ConversationEntity.STATUS_SENT),
        sender: Value(ConversationEntity.THEM),
        sendAt: Value(loop.receivedByTargetAt.toDouble()),
        receivedByServerAt: Value(loop.receivedByTargetAt.toDouble()),
        sentAt: Value(loop.receivedByTargetAt.toDouble()),
      ));
    });
  }

  Future<void> sendMessage(ChatUser interlocutor, Map<String, dynamic> data) async {
    var isContactExists = (await (select(interlocutorEntity)..where((it) => it.idUrl.equals(interlocutor.idUrl)))
        .getSingle()) != null;

    if(isContactExists){
      await (update(interlocutorEntity)..where((it) => it.idUrl.equals(interlocutor.idUrl))).write(
        InterlocutorEntityCompanion(
          lastChatId: Value(data["conversation_id"]),
          displayLastChat: Value(data["conversation"]),
          lastChatTime: Value(data["send_at"].toDouble()),
        ),
      );
    }else {
      await into(interlocutorEntity).insert(InterlocutorEntityCompanion(
        idUrl: Value(interlocutor.idUrl),
        username: Value(interlocutor.username),
        name: Value(interlocutor.name),
        profilePicture: Value(interlocutor.profilePicture),
        lastChatId: Value(data["conversation_id"]),
        displayLastChat: Value(data["conversation"]),
        lastChatTime: Value(data["send_at"].toDouble()),
      ));
    }

    await into(conversationEntity).insert(ConversationEntityCompanion(
      interlocutorId: Value(interlocutor.idUrl),
      conversationId: Value(data["conversation_id"]),
      conversation: Value(data["conversation"]),
      conversationStatus: Value(ConversationEntity.STATUS_SEND),
      sender: Value(ConversationEntity.ME),
      sendAt: Value(data["send_at"].toDouble()),
    ));
  }

  Future<void> readMessage(String senderId) async {
    await (update(conversationEntity)..where(
            (it) => it.interlocutorId.equals(senderId) & it.sender.equals(ConversationEntity.THEM)
    )).write(
      ConversationEntityCompanion(
        conversationStatus: Value(ConversationEntity.STATUS_READ),
        readAt: Value(DateTime.now().millisecondsSinceEpoch.toDouble()),
      ),
    );
  }

  Future<void> yourMessageReceivedByServer(ServerReceivedYourMessageResponse response) async {
    var isConversationExists = (await (select(conversationEntity)..where(
            (it) => it.conversationId.equals(response.conversationId))).getSingle()) != null;

    if(isConversationExists){
      await (update(conversationEntity)..where((it) => it.conversationId.equals(response.conversationId))).write(
        ConversationEntityCompanion(
          conversationStatus: Value(ConversationEntity.STATUS_RECEIVED),
          receivedByServerAt: Value(response.receivedByServerAt.toDouble()),
        ),
      );
    }
  }

  Future<void> yourMessageReceivedByTarget(TargetReceivedYourMessageResponse response) async {
    var isConversationExists = (await (select(conversationEntity)..where(
            (it) => it.conversationId.equals(response.conversationId))).getSingle()) != null;

    if(isConversationExists){
      await (update(conversationEntity)..where((it) => it.conversationId.equals(response.conversationId))).write(
        ConversationEntityCompanion(
          conversationStatus: Value(ConversationEntity.STATUS_SENT),
          sentAt: Value(response.receivedByTargetAt.toDouble()),
        ),
      );
    }
  }

  Future<void> yourMessageReadByTarget(TargetReadYourMessageResponse response) async {
    await (update(conversationEntity)..where((it) => it.interlocutorId.equals(response.receiverId))).write(
      ConversationEntityCompanion(
        conversationStatus: Value(ConversationEntity.STATUS_READ),
        readAt: Value(response.readAt.toDouble()),
      ),
    );
  }

  Future<void> updateStatusConversation(AskLatestMessageStatusResponse response) async {
    Future.forEach<AskLatestMessageStatusData>(response.data, (loop) async {
      if(loop.status == ConversationEntity.STATUS_SENT){
        await (update(conversationEntity)..where((it) => it.conversationId.equals(loop.idConversation))).write(
          ConversationEntityCompanion(
            conversationStatus: Value(ConversationEntity.STATUS_SENT),
            sentAt: Value(loop.sentAt),
          ),
        );
      }else if(loop.status == ConversationEntity.STATUS_READ){
        await (update(conversationEntity)..where((it) => it.conversationId.equals(loop.idConversation))).write(
          ConversationEntityCompanion(
            conversationStatus: Value(ConversationEntity.STATUS_READ),
            readAt: Value(loop.readAt),
          ),
        );
      }
    });
  }
}