// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Interlocutor extends DataClass implements Insertable<Interlocutor> {
  final String idUrl;
  final String name;
  final String username;
  final String profilePicture;
  final String lastChatId;
  final String displayLastChat;
  final double lastChatTime;
  Interlocutor(
      {@required this.idUrl,
      @required this.name,
      @required this.username,
      @required this.profilePicture,
      @required this.lastChatId,
      @required this.displayLastChat,
      @required this.lastChatTime});
  factory Interlocutor.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    return Interlocutor(
      idUrl:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}id_url']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      username: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}username']),
      profilePicture: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}profile_picture']),
      lastChatId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}last_chat_id']),
      displayLastChat: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}display_last_chat']),
      lastChatTime: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}last_chat_time']),
    );
  }
  factory Interlocutor.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Interlocutor(
      idUrl: serializer.fromJson<String>(json['idUrl']),
      name: serializer.fromJson<String>(json['name']),
      username: serializer.fromJson<String>(json['username']),
      profilePicture: serializer.fromJson<String>(json['profilePicture']),
      lastChatId: serializer.fromJson<String>(json['lastChatId']),
      displayLastChat: serializer.fromJson<String>(json['displayLastChat']),
      lastChatTime: serializer.fromJson<double>(json['lastChatTime']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'idUrl': serializer.toJson<String>(idUrl),
      'name': serializer.toJson<String>(name),
      'username': serializer.toJson<String>(username),
      'profilePicture': serializer.toJson<String>(profilePicture),
      'lastChatId': serializer.toJson<String>(lastChatId),
      'displayLastChat': serializer.toJson<String>(displayLastChat),
      'lastChatTime': serializer.toJson<double>(lastChatTime),
    };
  }

  @override
  InterlocutorEntityCompanion createCompanion(bool nullToAbsent) {
    return InterlocutorEntityCompanion(
      idUrl:
          idUrl == null && nullToAbsent ? const Value.absent() : Value(idUrl),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      username: username == null && nullToAbsent
          ? const Value.absent()
          : Value(username),
      profilePicture: profilePicture == null && nullToAbsent
          ? const Value.absent()
          : Value(profilePicture),
      lastChatId: lastChatId == null && nullToAbsent
          ? const Value.absent()
          : Value(lastChatId),
      displayLastChat: displayLastChat == null && nullToAbsent
          ? const Value.absent()
          : Value(displayLastChat),
      lastChatTime: lastChatTime == null && nullToAbsent
          ? const Value.absent()
          : Value(lastChatTime),
    );
  }

  Interlocutor copyWith(
          {String idUrl,
          String name,
          String username,
          String profilePicture,
          String lastChatId,
          String displayLastChat,
          double lastChatTime}) =>
      Interlocutor(
        idUrl: idUrl ?? this.idUrl,
        name: name ?? this.name,
        username: username ?? this.username,
        profilePicture: profilePicture ?? this.profilePicture,
        lastChatId: lastChatId ?? this.lastChatId,
        displayLastChat: displayLastChat ?? this.displayLastChat,
        lastChatTime: lastChatTime ?? this.lastChatTime,
      );
  @override
  String toString() {
    return (StringBuffer('Interlocutor(')
          ..write('idUrl: $idUrl, ')
          ..write('name: $name, ')
          ..write('username: $username, ')
          ..write('profilePicture: $profilePicture, ')
          ..write('lastChatId: $lastChatId, ')
          ..write('displayLastChat: $displayLastChat, ')
          ..write('lastChatTime: $lastChatTime')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      idUrl.hashCode,
      $mrjc(
          name.hashCode,
          $mrjc(
              username.hashCode,
              $mrjc(
                  profilePicture.hashCode,
                  $mrjc(
                      lastChatId.hashCode,
                      $mrjc(displayLastChat.hashCode,
                          lastChatTime.hashCode)))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Interlocutor &&
          other.idUrl == this.idUrl &&
          other.name == this.name &&
          other.username == this.username &&
          other.profilePicture == this.profilePicture &&
          other.lastChatId == this.lastChatId &&
          other.displayLastChat == this.displayLastChat &&
          other.lastChatTime == this.lastChatTime);
}

class InterlocutorEntityCompanion extends UpdateCompanion<Interlocutor> {
  final Value<String> idUrl;
  final Value<String> name;
  final Value<String> username;
  final Value<String> profilePicture;
  final Value<String> lastChatId;
  final Value<String> displayLastChat;
  final Value<double> lastChatTime;
  const InterlocutorEntityCompanion({
    this.idUrl = const Value.absent(),
    this.name = const Value.absent(),
    this.username = const Value.absent(),
    this.profilePicture = const Value.absent(),
    this.lastChatId = const Value.absent(),
    this.displayLastChat = const Value.absent(),
    this.lastChatTime = const Value.absent(),
  });
  InterlocutorEntityCompanion.insert({
    @required String idUrl,
    @required String name,
    @required String username,
    @required String profilePicture,
    @required String lastChatId,
    @required String displayLastChat,
    @required double lastChatTime,
  })  : idUrl = Value(idUrl),
        name = Value(name),
        username = Value(username),
        profilePicture = Value(profilePicture),
        lastChatId = Value(lastChatId),
        displayLastChat = Value(displayLastChat),
        lastChatTime = Value(lastChatTime);
  InterlocutorEntityCompanion copyWith(
      {Value<String> idUrl,
      Value<String> name,
      Value<String> username,
      Value<String> profilePicture,
      Value<String> lastChatId,
      Value<String> displayLastChat,
      Value<double> lastChatTime}) {
    return InterlocutorEntityCompanion(
      idUrl: idUrl ?? this.idUrl,
      name: name ?? this.name,
      username: username ?? this.username,
      profilePicture: profilePicture ?? this.profilePicture,
      lastChatId: lastChatId ?? this.lastChatId,
      displayLastChat: displayLastChat ?? this.displayLastChat,
      lastChatTime: lastChatTime ?? this.lastChatTime,
    );
  }
}

class $InterlocutorEntityTable extends InterlocutorEntity
    with TableInfo<$InterlocutorEntityTable, Interlocutor> {
  final GeneratedDatabase _db;
  final String _alias;
  $InterlocutorEntityTable(this._db, [this._alias]);
  final VerificationMeta _idUrlMeta = const VerificationMeta('idUrl');
  GeneratedTextColumn _idUrl;
  @override
  GeneratedTextColumn get idUrl => _idUrl ??= _constructIdUrl();
  GeneratedTextColumn _constructIdUrl() {
    return GeneratedTextColumn(
      'id_url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _usernameMeta = const VerificationMeta('username');
  GeneratedTextColumn _username;
  @override
  GeneratedTextColumn get username => _username ??= _constructUsername();
  GeneratedTextColumn _constructUsername() {
    return GeneratedTextColumn(
      'username',
      $tableName,
      false,
    );
  }

  final VerificationMeta _profilePictureMeta =
      const VerificationMeta('profilePicture');
  GeneratedTextColumn _profilePicture;
  @override
  GeneratedTextColumn get profilePicture =>
      _profilePicture ??= _constructProfilePicture();
  GeneratedTextColumn _constructProfilePicture() {
    return GeneratedTextColumn(
      'profile_picture',
      $tableName,
      false,
    );
  }

  final VerificationMeta _lastChatIdMeta = const VerificationMeta('lastChatId');
  GeneratedTextColumn _lastChatId;
  @override
  GeneratedTextColumn get lastChatId => _lastChatId ??= _constructLastChatId();
  GeneratedTextColumn _constructLastChatId() {
    return GeneratedTextColumn(
      'last_chat_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _displayLastChatMeta =
      const VerificationMeta('displayLastChat');
  GeneratedTextColumn _displayLastChat;
  @override
  GeneratedTextColumn get displayLastChat =>
      _displayLastChat ??= _constructDisplayLastChat();
  GeneratedTextColumn _constructDisplayLastChat() {
    return GeneratedTextColumn(
      'display_last_chat',
      $tableName,
      false,
    );
  }

  final VerificationMeta _lastChatTimeMeta =
      const VerificationMeta('lastChatTime');
  GeneratedRealColumn _lastChatTime;
  @override
  GeneratedRealColumn get lastChatTime =>
      _lastChatTime ??= _constructLastChatTime();
  GeneratedRealColumn _constructLastChatTime() {
    return GeneratedRealColumn(
      'last_chat_time',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        idUrl,
        name,
        username,
        profilePicture,
        lastChatId,
        displayLastChat,
        lastChatTime
      ];
  @override
  $InterlocutorEntityTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'interlocutor';
  @override
  final String actualTableName = 'interlocutor';
  @override
  VerificationContext validateIntegrity(InterlocutorEntityCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.idUrl.present) {
      context.handle(
          _idUrlMeta, idUrl.isAcceptableValue(d.idUrl.value, _idUrlMeta));
    } else if (isInserting) {
      context.missing(_idUrlMeta);
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (d.username.present) {
      context.handle(_usernameMeta,
          username.isAcceptableValue(d.username.value, _usernameMeta));
    } else if (isInserting) {
      context.missing(_usernameMeta);
    }
    if (d.profilePicture.present) {
      context.handle(
          _profilePictureMeta,
          profilePicture.isAcceptableValue(
              d.profilePicture.value, _profilePictureMeta));
    } else if (isInserting) {
      context.missing(_profilePictureMeta);
    }
    if (d.lastChatId.present) {
      context.handle(_lastChatIdMeta,
          lastChatId.isAcceptableValue(d.lastChatId.value, _lastChatIdMeta));
    } else if (isInserting) {
      context.missing(_lastChatIdMeta);
    }
    if (d.displayLastChat.present) {
      context.handle(
          _displayLastChatMeta,
          displayLastChat.isAcceptableValue(
              d.displayLastChat.value, _displayLastChatMeta));
    } else if (isInserting) {
      context.missing(_displayLastChatMeta);
    }
    if (d.lastChatTime.present) {
      context.handle(
          _lastChatTimeMeta,
          lastChatTime.isAcceptableValue(
              d.lastChatTime.value, _lastChatTimeMeta));
    } else if (isInserting) {
      context.missing(_lastChatTimeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idUrl};
  @override
  Interlocutor map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Interlocutor.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(InterlocutorEntityCompanion d) {
    final map = <String, Variable>{};
    if (d.idUrl.present) {
      map['id_url'] = Variable<String, StringType>(d.idUrl.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.username.present) {
      map['username'] = Variable<String, StringType>(d.username.value);
    }
    if (d.profilePicture.present) {
      map['profile_picture'] =
          Variable<String, StringType>(d.profilePicture.value);
    }
    if (d.lastChatId.present) {
      map['last_chat_id'] = Variable<String, StringType>(d.lastChatId.value);
    }
    if (d.displayLastChat.present) {
      map['display_last_chat'] =
          Variable<String, StringType>(d.displayLastChat.value);
    }
    if (d.lastChatTime.present) {
      map['last_chat_time'] = Variable<double, RealType>(d.lastChatTime.value);
    }
    return map;
  }

  @override
  $InterlocutorEntityTable createAlias(String alias) {
    return $InterlocutorEntityTable(_db, alias);
  }
}

class Conversation extends DataClass implements Insertable<Conversation> {
  final String interlocutorId;
  final String conversationId;
  final String conversation;
  final int conversationStatus;
  final int sender;
  final double sendAt;
  final double receivedByServerAt;
  final double sentAt;
  final double readAt;
  Conversation(
      {@required this.interlocutorId,
      @required this.conversationId,
      @required this.conversation,
      @required this.conversationStatus,
      @required this.sender,
      this.sendAt,
      this.receivedByServerAt,
      this.sentAt,
      this.readAt});
  factory Conversation.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final intType = db.typeSystem.forDartType<int>();
    final doubleType = db.typeSystem.forDartType<double>();
    return Conversation(
      interlocutorId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}interlocutor_id']),
      conversationId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}conversation_id']),
      conversation: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}conversation']),
      conversationStatus: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}conversation_status']),
      sender: intType.mapFromDatabaseResponse(data['${effectivePrefix}sender']),
      sendAt:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}send_at']),
      receivedByServerAt: doubleType.mapFromDatabaseResponse(
          data['${effectivePrefix}received_by_server_at']),
      sentAt:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}sent_at']),
      readAt:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}read_at']),
    );
  }
  factory Conversation.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Conversation(
      interlocutorId: serializer.fromJson<String>(json['interlocutorId']),
      conversationId: serializer.fromJson<String>(json['conversationId']),
      conversation: serializer.fromJson<String>(json['conversation']),
      conversationStatus: serializer.fromJson<int>(json['conversationStatus']),
      sender: serializer.fromJson<int>(json['sender']),
      sendAt: serializer.fromJson<double>(json['sendAt']),
      receivedByServerAt:
          serializer.fromJson<double>(json['receivedByServerAt']),
      sentAt: serializer.fromJson<double>(json['sentAt']),
      readAt: serializer.fromJson<double>(json['readAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'interlocutorId': serializer.toJson<String>(interlocutorId),
      'conversationId': serializer.toJson<String>(conversationId),
      'conversation': serializer.toJson<String>(conversation),
      'conversationStatus': serializer.toJson<int>(conversationStatus),
      'sender': serializer.toJson<int>(sender),
      'sendAt': serializer.toJson<double>(sendAt),
      'receivedByServerAt': serializer.toJson<double>(receivedByServerAt),
      'sentAt': serializer.toJson<double>(sentAt),
      'readAt': serializer.toJson<double>(readAt),
    };
  }

  @override
  ConversationEntityCompanion createCompanion(bool nullToAbsent) {
    return ConversationEntityCompanion(
      interlocutorId: interlocutorId == null && nullToAbsent
          ? const Value.absent()
          : Value(interlocutorId),
      conversationId: conversationId == null && nullToAbsent
          ? const Value.absent()
          : Value(conversationId),
      conversation: conversation == null && nullToAbsent
          ? const Value.absent()
          : Value(conversation),
      conversationStatus: conversationStatus == null && nullToAbsent
          ? const Value.absent()
          : Value(conversationStatus),
      sender:
          sender == null && nullToAbsent ? const Value.absent() : Value(sender),
      sendAt:
          sendAt == null && nullToAbsent ? const Value.absent() : Value(sendAt),
      receivedByServerAt: receivedByServerAt == null && nullToAbsent
          ? const Value.absent()
          : Value(receivedByServerAt),
      sentAt:
          sentAt == null && nullToAbsent ? const Value.absent() : Value(sentAt),
      readAt:
          readAt == null && nullToAbsent ? const Value.absent() : Value(readAt),
    );
  }

  Conversation copyWith(
          {String interlocutorId,
          String conversationId,
          String conversation,
          int conversationStatus,
          int sender,
          double sendAt,
          double receivedByServerAt,
          double sentAt,
          double readAt}) =>
      Conversation(
        interlocutorId: interlocutorId ?? this.interlocutorId,
        conversationId: conversationId ?? this.conversationId,
        conversation: conversation ?? this.conversation,
        conversationStatus: conversationStatus ?? this.conversationStatus,
        sender: sender ?? this.sender,
        sendAt: sendAt ?? this.sendAt,
        receivedByServerAt: receivedByServerAt ?? this.receivedByServerAt,
        sentAt: sentAt ?? this.sentAt,
        readAt: readAt ?? this.readAt,
      );
  @override
  String toString() {
    return (StringBuffer('Conversation(')
          ..write('interlocutorId: $interlocutorId, ')
          ..write('conversationId: $conversationId, ')
          ..write('conversation: $conversation, ')
          ..write('conversationStatus: $conversationStatus, ')
          ..write('sender: $sender, ')
          ..write('sendAt: $sendAt, ')
          ..write('receivedByServerAt: $receivedByServerAt, ')
          ..write('sentAt: $sentAt, ')
          ..write('readAt: $readAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      interlocutorId.hashCode,
      $mrjc(
          conversationId.hashCode,
          $mrjc(
              conversation.hashCode,
              $mrjc(
                  conversationStatus.hashCode,
                  $mrjc(
                      sender.hashCode,
                      $mrjc(
                          sendAt.hashCode,
                          $mrjc(receivedByServerAt.hashCode,
                              $mrjc(sentAt.hashCode, readAt.hashCode)))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Conversation &&
          other.interlocutorId == this.interlocutorId &&
          other.conversationId == this.conversationId &&
          other.conversation == this.conversation &&
          other.conversationStatus == this.conversationStatus &&
          other.sender == this.sender &&
          other.sendAt == this.sendAt &&
          other.receivedByServerAt == this.receivedByServerAt &&
          other.sentAt == this.sentAt &&
          other.readAt == this.readAt);
}

class ConversationEntityCompanion extends UpdateCompanion<Conversation> {
  final Value<String> interlocutorId;
  final Value<String> conversationId;
  final Value<String> conversation;
  final Value<int> conversationStatus;
  final Value<int> sender;
  final Value<double> sendAt;
  final Value<double> receivedByServerAt;
  final Value<double> sentAt;
  final Value<double> readAt;
  const ConversationEntityCompanion({
    this.interlocutorId = const Value.absent(),
    this.conversationId = const Value.absent(),
    this.conversation = const Value.absent(),
    this.conversationStatus = const Value.absent(),
    this.sender = const Value.absent(),
    this.sendAt = const Value.absent(),
    this.receivedByServerAt = const Value.absent(),
    this.sentAt = const Value.absent(),
    this.readAt = const Value.absent(),
  });
  ConversationEntityCompanion.insert({
    @required String interlocutorId,
    @required String conversationId,
    @required String conversation,
    @required int conversationStatus,
    @required int sender,
    this.sendAt = const Value.absent(),
    this.receivedByServerAt = const Value.absent(),
    this.sentAt = const Value.absent(),
    this.readAt = const Value.absent(),
  })  : interlocutorId = Value(interlocutorId),
        conversationId = Value(conversationId),
        conversation = Value(conversation),
        conversationStatus = Value(conversationStatus),
        sender = Value(sender);
  ConversationEntityCompanion copyWith(
      {Value<String> interlocutorId,
      Value<String> conversationId,
      Value<String> conversation,
      Value<int> conversationStatus,
      Value<int> sender,
      Value<double> sendAt,
      Value<double> receivedByServerAt,
      Value<double> sentAt,
      Value<double> readAt}) {
    return ConversationEntityCompanion(
      interlocutorId: interlocutorId ?? this.interlocutorId,
      conversationId: conversationId ?? this.conversationId,
      conversation: conversation ?? this.conversation,
      conversationStatus: conversationStatus ?? this.conversationStatus,
      sender: sender ?? this.sender,
      sendAt: sendAt ?? this.sendAt,
      receivedByServerAt: receivedByServerAt ?? this.receivedByServerAt,
      sentAt: sentAt ?? this.sentAt,
      readAt: readAt ?? this.readAt,
    );
  }
}

class $ConversationEntityTable extends ConversationEntity
    with TableInfo<$ConversationEntityTable, Conversation> {
  final GeneratedDatabase _db;
  final String _alias;
  $ConversationEntityTable(this._db, [this._alias]);
  final VerificationMeta _interlocutorIdMeta =
      const VerificationMeta('interlocutorId');
  GeneratedTextColumn _interlocutorId;
  @override
  GeneratedTextColumn get interlocutorId =>
      _interlocutorId ??= _constructInterlocutorId();
  GeneratedTextColumn _constructInterlocutorId() {
    return GeneratedTextColumn(
      'interlocutor_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _conversationIdMeta =
      const VerificationMeta('conversationId');
  GeneratedTextColumn _conversationId;
  @override
  GeneratedTextColumn get conversationId =>
      _conversationId ??= _constructConversationId();
  GeneratedTextColumn _constructConversationId() {
    return GeneratedTextColumn(
      'conversation_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _conversationMeta =
      const VerificationMeta('conversation');
  GeneratedTextColumn _conversation;
  @override
  GeneratedTextColumn get conversation =>
      _conversation ??= _constructConversation();
  GeneratedTextColumn _constructConversation() {
    return GeneratedTextColumn(
      'conversation',
      $tableName,
      false,
    );
  }

  final VerificationMeta _conversationStatusMeta =
      const VerificationMeta('conversationStatus');
  GeneratedIntColumn _conversationStatus;
  @override
  GeneratedIntColumn get conversationStatus =>
      _conversationStatus ??= _constructConversationStatus();
  GeneratedIntColumn _constructConversationStatus() {
    return GeneratedIntColumn(
      'conversation_status',
      $tableName,
      false,
    );
  }

  final VerificationMeta _senderMeta = const VerificationMeta('sender');
  GeneratedIntColumn _sender;
  @override
  GeneratedIntColumn get sender => _sender ??= _constructSender();
  GeneratedIntColumn _constructSender() {
    return GeneratedIntColumn(
      'sender',
      $tableName,
      false,
    );
  }

  final VerificationMeta _sendAtMeta = const VerificationMeta('sendAt');
  GeneratedRealColumn _sendAt;
  @override
  GeneratedRealColumn get sendAt => _sendAt ??= _constructSendAt();
  GeneratedRealColumn _constructSendAt() {
    return GeneratedRealColumn(
      'send_at',
      $tableName,
      true,
    );
  }

  final VerificationMeta _receivedByServerAtMeta =
      const VerificationMeta('receivedByServerAt');
  GeneratedRealColumn _receivedByServerAt;
  @override
  GeneratedRealColumn get receivedByServerAt =>
      _receivedByServerAt ??= _constructReceivedByServerAt();
  GeneratedRealColumn _constructReceivedByServerAt() {
    return GeneratedRealColumn(
      'received_by_server_at',
      $tableName,
      true,
    );
  }

  final VerificationMeta _sentAtMeta = const VerificationMeta('sentAt');
  GeneratedRealColumn _sentAt;
  @override
  GeneratedRealColumn get sentAt => _sentAt ??= _constructSentAt();
  GeneratedRealColumn _constructSentAt() {
    return GeneratedRealColumn(
      'sent_at',
      $tableName,
      true,
    );
  }

  final VerificationMeta _readAtMeta = const VerificationMeta('readAt');
  GeneratedRealColumn _readAt;
  @override
  GeneratedRealColumn get readAt => _readAt ??= _constructReadAt();
  GeneratedRealColumn _constructReadAt() {
    return GeneratedRealColumn(
      'read_at',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        interlocutorId,
        conversationId,
        conversation,
        conversationStatus,
        sender,
        sendAt,
        receivedByServerAt,
        sentAt,
        readAt
      ];
  @override
  $ConversationEntityTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'conversation';
  @override
  final String actualTableName = 'conversation';
  @override
  VerificationContext validateIntegrity(ConversationEntityCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.interlocutorId.present) {
      context.handle(
          _interlocutorIdMeta,
          interlocutorId.isAcceptableValue(
              d.interlocutorId.value, _interlocutorIdMeta));
    } else if (isInserting) {
      context.missing(_interlocutorIdMeta);
    }
    if (d.conversationId.present) {
      context.handle(
          _conversationIdMeta,
          conversationId.isAcceptableValue(
              d.conversationId.value, _conversationIdMeta));
    } else if (isInserting) {
      context.missing(_conversationIdMeta);
    }
    if (d.conversation.present) {
      context.handle(
          _conversationMeta,
          conversation.isAcceptableValue(
              d.conversation.value, _conversationMeta));
    } else if (isInserting) {
      context.missing(_conversationMeta);
    }
    if (d.conversationStatus.present) {
      context.handle(
          _conversationStatusMeta,
          conversationStatus.isAcceptableValue(
              d.conversationStatus.value, _conversationStatusMeta));
    } else if (isInserting) {
      context.missing(_conversationStatusMeta);
    }
    if (d.sender.present) {
      context.handle(
          _senderMeta, sender.isAcceptableValue(d.sender.value, _senderMeta));
    } else if (isInserting) {
      context.missing(_senderMeta);
    }
    if (d.sendAt.present) {
      context.handle(
          _sendAtMeta, sendAt.isAcceptableValue(d.sendAt.value, _sendAtMeta));
    }
    if (d.receivedByServerAt.present) {
      context.handle(
          _receivedByServerAtMeta,
          receivedByServerAt.isAcceptableValue(
              d.receivedByServerAt.value, _receivedByServerAtMeta));
    }
    if (d.sentAt.present) {
      context.handle(
          _sentAtMeta, sentAt.isAcceptableValue(d.sentAt.value, _sentAtMeta));
    }
    if (d.readAt.present) {
      context.handle(
          _readAtMeta, readAt.isAcceptableValue(d.readAt.value, _readAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {conversationId};
  @override
  Conversation map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Conversation.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(ConversationEntityCompanion d) {
    final map = <String, Variable>{};
    if (d.interlocutorId.present) {
      map['interlocutor_id'] =
          Variable<String, StringType>(d.interlocutorId.value);
    }
    if (d.conversationId.present) {
      map['conversation_id'] =
          Variable<String, StringType>(d.conversationId.value);
    }
    if (d.conversation.present) {
      map['conversation'] = Variable<String, StringType>(d.conversation.value);
    }
    if (d.conversationStatus.present) {
      map['conversation_status'] =
          Variable<int, IntType>(d.conversationStatus.value);
    }
    if (d.sender.present) {
      map['sender'] = Variable<int, IntType>(d.sender.value);
    }
    if (d.sendAt.present) {
      map['send_at'] = Variable<double, RealType>(d.sendAt.value);
    }
    if (d.receivedByServerAt.present) {
      map['received_by_server_at'] =
          Variable<double, RealType>(d.receivedByServerAt.value);
    }
    if (d.sentAt.present) {
      map['sent_at'] = Variable<double, RealType>(d.sentAt.value);
    }
    if (d.readAt.present) {
      map['read_at'] = Variable<double, RealType>(d.readAt.value);
    }
    return map;
  }

  @override
  $ConversationEntityTable createAlias(String alias) {
    return $ConversationEntityTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $InterlocutorEntityTable _interlocutorEntity;
  $InterlocutorEntityTable get interlocutorEntity =>
      _interlocutorEntity ??= $InterlocutorEntityTable(this);
  $ConversationEntityTable _conversationEntity;
  $ConversationEntityTable get conversationEntity =>
      _conversationEntity ??= $ConversationEntityTable(this);
  ListContactResult _rowToListContactResult(QueryRow row) {
    return ListContactResult(
      idUrl: row.readString('id_url'),
      username: row.readString('username'),
      name: row.readString('name'),
      profilePicture: row.readString('profile_picture'),
      lastChatTime: row.readDouble('last_chat_time'),
      displayLastChat: row.readString('display_last_chat'),
      conversationStatus: row.readInt('conversation_status'),
      sender: row.readInt('sender'),
      unreadCount: row.readInt('unread_count'),
    );
  }

  Selectable<ListContactResult> listContactQuery() {
    return customSelectQuery(
        'SELECT i.id_url, i.username, i.name, i.profile_picture, i.last_chat_time, i.display_last_chat, c.conversation_status, c.sender, (SELECT COUNT(*) FROM conversation cv WHERE cv.interlocutor_id = i.id_url AND cv.sender = -2 AND cv.conversation_status != 3) AS unread_count FROM interlocutor i LEFT JOIN conversation c ON i.last_chat_id = c.conversation_id ORDER BY i.last_chat_time DESC',
        variables: [],
        readsFrom: {
          interlocutorEntity,
          conversationEntity
        }).map(_rowToListContactResult);
  }

  Future<List<ListContactResult>> listContact() {
    return listContactQuery().get();
  }

  Stream<List<ListContactResult>> watchListContact() {
    return listContactQuery().watch();
  }

  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [interlocutorEntity, conversationEntity];
}

class ListContactResult {
  final String idUrl;
  final String username;
  final String name;
  final String profilePicture;
  final double lastChatTime;
  final String displayLastChat;
  final int conversationStatus;
  final int sender;
  final int unreadCount;
  ListContactResult({
    this.idUrl,
    this.username,
    this.name,
    this.profilePicture,
    this.lastChatTime,
    this.displayLastChat,
    this.conversationStatus,
    this.sender,
    this.unreadCount,
  });
}
