import 'dart:convert';

import 'package:halokes_les_guru/ancestor/BaseRepository.dart';
import 'package:halokes_les_guru/ancestor/BaseResponse.dart';
import 'package:halokes_les_guru/ancestor/BaseState.dart';
import 'package:halokes_les_guru/network/response/general/ChatUserResponse.dart';
import 'package:halokes_les_guru/network/response/general/JadwalMengajarResponse.dart';
import 'package:halokes_les_guru/network/response/general/LastSeenResponse.dart';
import 'package:halokes_les_guru/network/response/general/LoginResponse.dart';
import 'package:halokes_les_guru/network/response/general/MapelResponse.dart';
import 'package:halokes_les_guru/preference/AppPreference.dart';


///
/// All request to APIs/Webservices should written here
/// and the response should written in network/response directory
///
class GeneralRepository extends BaseRepository{

  GeneralRepository(BaseState baseState) : super(baseState, baseUrl: "http://192.168.1.111:8888/halokes-honorer/index.php/api/");

  Future<String> get _idUrl async {
    return (await AppPreference.getUser()).idGuruUrl;
  }

  void executeRegister(int typeRequest, Map<String, dynamic> params, Function(BaseResponse) completion) async {
    var response = await post("guru/register", params, typeRequest);

    if(response != null){
      completion(BaseResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeLogin(int typeRequest, Map<String, dynamic> params, Function(LoginResponse) completion) async {
    var response = await post("guru/login", params, typeRequest);

    if(response != null){
      completion(LoginResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetMapel(int typeRequest, Function(MapelResponse) completion) async {
    var response = await get("mapel/guru/${await _idUrl}", null, typeRequest);

    if(response != null){
      completion(MapelResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeBuatJadwal(int typeRequest, Map<String, dynamic> params, Function(BaseResponse) completion) async {
    var response = await post("jadwal/tambah", params, typeRequest);

    if(response != null){
      completion(BaseResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetJadwal(int typeRequest, Function(JadwalMengajarResponse) completion) async {
    var response = await get("jadwal/guru/${await _idUrl}", null, typeRequest);

    if(response != null){
      completion(JadwalMengajarResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetLastSeen(int typeRequest, String userId, Function(LastSeenResponse) completion) async {
    var response = await post("chat/lastseen/user/$userId", null, typeRequest);

    if(response != null){
      completion(LastSeenResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeGetChatUser(int typeRequest, Function(ChatUserResponse) completion) async {
    var response = await post("chat/user/${await _idUrl}", null, typeRequest);

    if(response != null){
      completion(ChatUserResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeChangePassword(int typeRequest, Map<String, dynamic> params, Function(ChatUserResponse) completion) async {
    var response = await post("change_password/${await _idUrl}", params, typeRequest);

    if(response != null){
      completion(ChatUserResponse.fromJson(jsonDecode(response.data)));
    }
  }

  void executeUpdateProfile(int typeRequest, Function(ChatUserResponse) completion) async {
    var response = await post("chat/user/${await _idUrl}", null, typeRequest);

    if(response != null){
      completion(ChatUserResponse.fromJson(jsonDecode(response.data)));
    }
  }
}