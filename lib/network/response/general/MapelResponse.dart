import 'package:halokes_les_guru/ancestor/BaseResponse.dart';

class MapelResponse extends BaseResponse {
  List<MapelData> data = List();

  MapelResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    json["data"]?.forEach((it) => data.add(MapelData.fromJson(it)));
  }
}

class MapelData {
  String idMapelUrl;
  String mapelNama;

  MapelData.fromJson(Map<String, dynamic> json){
    idMapelUrl = json["id_mapel_url"] ?? "";
    mapelNama = json["mapel_nama"] ?? "";
  }
}