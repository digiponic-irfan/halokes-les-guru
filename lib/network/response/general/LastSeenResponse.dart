import 'package:halokes_les_guru/ancestor/BaseResponse.dart';

class LastSeenResponse extends BaseResponse {
  String lastSeen;

  LastSeenResponse.fromJson(Map<String, dynamic> json){
    lastSeen = json["last_seen"] ?? "";
  }
}