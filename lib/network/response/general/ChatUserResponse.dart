import 'package:halokes_les_guru/ancestor/BaseResponse.dart';
import 'package:halokes_les_guru/database/database.dart';
import 'package:intl/intl.dart';

class ChatUserResponse extends BaseResponse {
  List<ChatUser> data = List();

  ChatUserResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    json["data"]?.forEach((it) => data.add(ChatUser.fromJson(it)));
  }
}

class ChatUser {
  String idUser;
  String username;
  String profilePicture;
  String role;
  String idUrl;
  String name;
  String school;
  String lastLogout;
  int statusLogin;

  String displayOnline = "";

  ChatUser.fromJson(Map<String, dynamic> json){
    idUser = json["id_user"] ?? "";
    username = json["username"] ?? "";
    profilePicture = json["profile_picture"] ?? "";
    role = json["role"] ?? "";
    idUrl = json["id_url"] ?? "";
    name = json["name"] ?? "";
    school = json["school"] ?? "";

    if(json["last_logout"] != null){
      var today = DateTime.now();
      var lastSeen = DateFormat("yyyy-MM-dd HH:mm:ss").parse(json["last_logout"]);
      var differDays = today.weekday - lastSeen.weekday;

      if(today.year == lastSeen.year && today.month == lastSeen.month){
        if(differDays == 1){
          lastLogout = "Terakhir dilihat kemarin ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else if(differDays == 0){
          lastLogout = "Terakhir dilihat hari ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }else {
          lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
        }
      }else {
        lastLogout = "Terakhir dilihat ${DateFormat("dd MMM").format(lastSeen)} ini pukul ${DateFormat("HH:mm").format(lastSeen)}";
      }
    }else {
      lastLogout = "";
    }
    statusLogin = json["status_login"] != null ? int.parse(json["status_login"]) : 0;

    displayOnline = statusLogin == 0 ? "$lastLogout" : "Online";
  }

  ChatUser.fromListContact(ListContactResult contact){
    idUrl = contact.idUrl;
    name = contact.name;
    profilePicture = contact.profilePicture;
    displayOnline = "";
  }
}