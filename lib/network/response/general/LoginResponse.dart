import 'dart:convert';

import 'package:halokes_les_guru/ancestor/BaseResponse.dart';

class LoginResponse extends BaseResponse {
  UserData data;

  LoginResponse.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    data = UserData.fromJson(json["data"]);
  }
}

class UserData {
  String idUserUrl;
  String idGuru;
  String idGuruUrl;
  String email;
  String profilePicture;
  String role;
  String guruNama;
  String guruAsalSekolah;

  UserData.fromJson(Map<String, dynamic> json){
    idUserUrl = json["id_user_url"] ?? "";
    idGuru = json["id_guru"] ?? "";
    idGuruUrl = json["id_guru_url"] ?? "";
    email = json["email"] ?? "";
    profilePicture = json["profile_picture"] ?? "";
    role = json["role"] ?? "";
    guruNama = json["guru_nama"] ?? "";
    guruAsalSekolah = json["guru_asal_sekolah"] ?? "";
  }

  String toJson() => jsonEncode({
    "id_user_url" : idUserUrl,
    "id_guru" : idGuru,
    "id_guru_url" : idGuruUrl,
    "email" : email,
    "profile_picture" : profilePicture,
    "role" : role,
    "guru_nama" : guruNama,
    "guru_asal_sekolah" : guruAsalSekolah
  });
}