import 'package:halokes_les_guru/ancestor/BaseResponse.dart';

class JadwalMengajarResponse extends BaseResponse {
  List<JadwalMengajarData> data = List();

  JadwalMengajarResponse.fromJson(Map<String, dynamic> json): super.fromJson(json){
    json["data"]?.forEach((it) => data.add(JadwalMengajarData.fromJson(it)));
  }
}

class JadwalMengajarData {
  String mapelNama;
  String materi;
  String guruNama;
  String tanggal;
  int kelas;
  String jamMulai;
  String jamSelesai;
  String jumlahPertemuan;
  String harga;

  JadwalMengajarData.fromJson(Map<String, dynamic> json){
    mapelNama = json["mapel_nama"] ?? "";
    materi = json["materi"] ?? "";
    guruNama = json["guru_nama"] ?? "";
    tanggal = json["tanggal"] ?? "";
    kelas = json["kelas"] != null ? int.parse(json["kelas"]) : 0;
    jamMulai = json["jam_mulai"] ?? "";
    jamSelesai = json["jam_selesai"] ?? "";
    jumlahPertemuan = json["jumlah_pertemuan"] ?? "";
    harga = json["harga"] ?? "";
  }
}