import 'package:halokes_les_guru/database/entities/Conversation.dart';

class AskLatestMessageStatusResponse {
  List<AskLatestMessageStatusData> data = List();

  AskLatestMessageStatusResponse.fromJson(Map<String, dynamic> json){
    if(json["data"] != null){
      json["data"].forEach((it) => data.add(AskLatestMessageStatusData.fromJson(it)));
    }
  }
}

class AskLatestMessageStatusData {
  String idConversation;
  double sentAt;
  double readAt;
  int status;

  AskLatestMessageStatusData.fromJson(Map<String, dynamic> json){
    idConversation = json["id_conversation"] ?? "";
    sentAt = json["sent_at"] != null ? double.parse(json["sent_at"]) : null;
    readAt = json["read_at"] != null ? double.parse(json["read_at"]) : null;
    status = int.parse(json["status"]) ?? ConversationEntity.STATUS_RECEIVED;
  }
}