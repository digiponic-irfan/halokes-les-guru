class SomeoneSendMessageResponse {
  String conversationId;
  String senderUrlId;
  String receiverUrlId;
  String conversation;
  int receivedByServerAt;
  String senderName;
  String senderFoto;
  String senderUsername;

  SomeoneSendMessageResponse.fromJson(Map<String, dynamic> json){
    conversationId = json["conversation_id"] ?? "";
    senderUrlId = json["sender_url_id"] ?? "";
    receiverUrlId = json["receiver_url_id"] ?? "";
    conversation = json["conversation"] ?? "";
    receivedByServerAt = json["received_by_server_at"] ?? 0;
    senderName = json["sender"]["name"] ?? "";
    senderFoto = json["sender"]["profile_picture"] ?? "";
    senderUsername = json["sender"]["username"];
  }
}