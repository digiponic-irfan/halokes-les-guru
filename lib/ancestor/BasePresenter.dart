import 'package:halokes_les_guru/database/database.dart';
import 'package:halokes_les_guru/network/request/GeneralRepository.dart';
import 'package:halokes_les_guru/network/request/SocketChatRepository.dart';

import 'BaseState.dart';


///
/// You should extends this class when creating another Presenter Class
///
class BasePresenter {
  final BaseState state;

  MyDatabase database;

  GeneralRepository generalRepo;

  SocketChatRepository chatSocketRepo;

  BasePresenter(this.state){
    database = MyDatabase.instance();

    generalRepo = GeneralRepository(state);
    chatSocketRepo = SocketChatRepository.instance;
  }

}