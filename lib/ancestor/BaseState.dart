import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:halokes_les_guru/ancestor/BaseResponse.dart';
import 'package:halokes_les_guru/custom/dialog/LoadingDialog.dart';
import 'package:halokes_les_guru/extension/ErrorMessaging.dart';

///
/// You should extends this class when creating another State class of StatefulWidget
///
/// Note : A Widget should contain
///
/// 1. Stateless/Stateful class, if Stateful it's State should extends BaseState class
/// 2. Presenter class, it should extends BasePresenter class
/// 3. Delegate class, the State class should implements it
///
/// Use Case :
/// 1. State class want to get data from APIs/ Webservices, so it calls Presenter class that
/// contains RemoteRepository variable
/// 2. State class creating Presenter variable class and pass Delegate class to it
/// 3. Presenter send request to APIs/Webservices by repository.someRequest()
/// 4. Presenter send data to Delegate class by delegate.onSomeRequestSuccess(data)
/// 5. State class obtaining the data
///
abstract class BaseState<T extends StatefulWidget> extends State<T> {
  static const DEFAULT_ALERT_DIALOG_TAG = -1;
  static const DEFAULT_DIALOG_TAG = -2;
  static const DEFAULT_DATE_PICKER_TAG = -3;

  @mustCallSuper
  @override
  void initState() {
    super.initState();
    runOnInitState();
  }

  void runOnInitState(){
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      afterWidgetBuilt();
    });
  }

  ///
  /// Called when widget already built
  /// Requesting to APIs/Webservices should happens here
  ///
  void afterWidgetBuilt(){

  }

  ///
  /// Called if Second Widget was finished
  ///
  /// Use case :
  /// - First widget calls navigateTo(Second Widget)
  /// - Second widget calls Navigator.pop() or finish()
  /// - onNavigationResume called in First Widget
  ///
  /// @params from = TAG of Second Widget that passed to navigateTo()
  ///
  /// Note : This will only called if you change widget by calling navigateTo, not by calling Navigator.of.push()
  ///
  /// ex:
  /// class FirstWidget extends BaseState{
  ///   @override
  ///   onNavigationResume(String from){
  ///     if(from == MyApp.ROUTE_SECOND_WIDGET){
  ///       do something here
  ///     }
  ///   }
  ///
  ///   Widget build(){
  ///     return Button(
  ///       onPressed: () => navigateTo(MyApp.ROUTE_SECOND_WIDGET);
  ///     );
  ///   }
  /// }
  ///
  /// class SecondWidget extends BaseState {
  ///   @override
  ///   Widget build(){
  ///     return Button(
  ///       onPressed: () => finish();
  ///     )
  ///   }
  /// }
  ///
  void onNavigationResume(String from){

  }


  ///
  /// Called before change navigation,
  /// only called if singleTop = false
  ///
  void onNavigationPaused(String target){

  }

  ///
  /// Like onNavigationResume but
  /// only called if Second Widget returning object on Navigator.pop() or finish()
  ///
  /// @params from = TAG of Second Widget that passed to navigateTo()
  /// @params result = Returned object from Second Widget
  ///
  /// Note : onNavigationResume will always called whether Second Widget returning object or not
  /// Note : This will only called if you change widget by calling navigateTo, not by calling Navigator.of.push()
  ///
  /// ex:
  /// class FirstWidget extends BaseState{
  ///   @override
  ///   onNavigationResume(String from){
  ///     if(from == MyApp.ROUTE_SECOND_WIDGET){
  ///       do something here
  ///     }
  ///   }
  ///
  ///   @override
  ///   onNavigationResult(String from, dynamic result){
  ///     if(from == MyApp.ROUTE_SECOND_WIDGET){
  ///       if(result == "YES"){
  ///         do something here
  ///       }
  ///     }
  ///   }
  ///
  ///   @override
  ///   Widget build(){
  ///     return Button(
  ///       onPressed: () => navigateTo(MyApp.ROUTE_SECOND_WIDGET);
  ///     );
  ///   }
  /// }
  ///
  /// class SecondWidget extends BaseState {
  ///   @override
  ///   Widget build(){
  ///     return Button(
  ///       onPressed: () => finish(result: "YES");
  ///     )
  ///   }
  /// }
  void onNavigationResult(String from, dynamic resultData){

  }

  ///
  /// Navigate to another widget
  /// @params destination = TAG of Target Widget, ex : MyApp.ROUTE_HOME
  /// @params singleTop
  /// if true it will close all previous widget, just like Android's finishAffinity()
  /// @params arguments = Argument that will be retrieved in Target Widget
  ///
  /// ex:
  /// class FirstWidget extends BaseState{
  ///   @override
  ///   onNavigationResume(String from){
  ///     if(from == MyApp.ROUTE_SECOND_WIDGET){
  ///       do something here
  ///     }
  ///   }
  ///
  ///   Widget build(){
  ///     return Button(
  ///       onPressed: () => navigateTo(MyApp.ROUTE_SECOND_WIDGET, argument: TargetArgument());
  ///     );
  ///   }
  /// }
  ///
  /// class TargetArgument {
  ///   some variable here
  /// }
  ///
  /// class TargetWidget extends StatefulWidget{
  ///   final TargetArgument argument;
  ///   TargetWidget({this.argument});
  ///
  ///   _TargetWidget state => _TargetWidget();
  /// }
  ///
  /// class _TargetWidget extends BaseState {
  ///   @override
  ///   initState(){
  ///     you can retrieve argument here with "widget.argument"
  ///   }
  ///
  ///   @override
  ///   Widget build(BuildContext context){
  ///     or assign it to widget
  ///
  ///     return Text(widget.argument.someVariable);
  ///   }
  /// }
  ///
  /// Note : You need to manually assign argument
  /// it often written in MaterialApp.onGenerateRoute like this
  ///
  /// MaterialApp(
  ///   onGenerateRoute: (settings){
  ///     switch(settings.name){
  ///       case ROUTE_FIRST_WIDGET:
  ///         return PageRoute(builder: (ctx) => FirstWidget());
  ///       case ROUTE_TARGET_WIDGET:
  ///         assign argument like this
  ///         return PageRoute(builder: (ctx) => TargetWidget(argument: settings.argument));
  ///     }
  ///   }
  /// );
  ///
  ///
  void navigateTo(String destination, {bool singleTop = false, dynamic arguments}) async {
    if(singleTop){
      Navigator.of(context).pushNamedAndRemoveUntil(destination, (_) => false, arguments: arguments);
    }else {
      onNavigationPaused(destination);

      var result = await Navigator.of(context)
          .pushNamed(destination, arguments: arguments);

      if(result != null){
        onNavigationResult(destination, result);
      }
      onNavigationResume(destination);
    }
  }

  ///
  /// Same as navigateTo but
  /// this method closes current widget
  /// and then show target widget
  ///
  /// @params destination = TAG of Target Widget
  /// @params argument = Argument that will be retrieved in Target Widget
  ///
  void navigateAndFinishCurrent(String destination, {dynamic arguments}) async {
    Navigator.of(context).popAndPushNamed(destination, arguments: arguments);
  }

  ///
  /// Finishing current widget and show previous widget
  /// @params result = Object that will be passed to previous widget
  ///
  /// you can get the result by overriding onNavigationResult()
  /// it will only called if result not null
  ///
  /// or you can do something in previous widget by overriding onNavigationResume()
  ///
  ///
  void finish({dynamic result}){
    Navigator.of(context).pop(result);
  }

  ///
  /// Open dialog
  /// if Android it will open MaterialDialog Theme
  /// if iOS it will open CupertinoDialog Theme
  ///
  /// @params tag = TAG of the dialog
  /// @params context = BuildContext of widget
  /// @params builder = Builder of dialog
  ///
  /// ex:
  /// class DialogWidget extends BaseState{
  ///   static const DIALOG_TAG = 0;
  ///
  ///   @override
  ///   Widget build(BuildContext context){
  ///     return Button(
  ///       onPressed: () => openDialog(
  ///         tag: DIALOG_TAG,
  ///         context: context,
  ///         builder: (context) => DialogContent()
  ///       )
  ///     )
  ///   }
  /// }
  ///
  /// class DialogContent extends StatelessWidget{
  ///   @override
  ///   Widget build(BuildContext context){
  ///     return Dialog(
  ///       child: Container(
  ///         child Center(
  ///           child: Button(
  ///             onPressed: () => Navigator.of(context).pop()
  ///           )
  ///         )
  ///       )
  ///     )
  ///   }
  /// }
  ///
  /// You can receive returned object from dialog by overriding onDialogResult()
  /// it will only called if returned object not null.
  ///
  /// or overriding onDialogClosed() if you want do something when dialog was closed
  ///
  void openDialog({@required dynamic tag,
    @required BuildContext context,
    @required Widget Function(BuildContext context) builder}) async {

    dynamic result;

    if(Platform.isIOS){
      result = await showCupertinoDialog(
        context: context,
        builder: builder,
      );
    }else {
      result = await showDialog(
        context: context,
        builder: builder,
      );
    }

    if(result != null){
      onDialogResult(tag, result);
    }
    onDialogClosed(tag);
  }

  ///
  /// Open date picker dialog
  /// If date selected you can retrieve selected date
  /// by overriding onDialogResult(), if no date selected
  /// you can detect dialog close by overriding onDialogClosed()
  ///
  /// @params tag = TAG of the dialog
  /// other @params just like normal date picker
  ///
  void openDatePicker({dynamic tag = DEFAULT_DATE_PICKER_TAG,
    @required BuildContext context, DateTime initialDate,
    DateTime firstDate, DateTime lastDate,
    SelectableDayPredicate selectableDayPredicate,
    DatePickerMode initialDatePickerMode = DatePickerMode.day,
    Locale locale,
    TextDirection textDirection,
    TransitionBuilder builder,
    bool useRootNavigator = true}) async {
    var _firstDate = firstDate ?? DateTime(1970);
    var _initialDate = initialDate ?? DateTime.now();
    var _lastDate = lastDate ?? DateTime(DateTime.now().year + 10);

    var result = await showDatePicker(context: context,
        initialDate: _initialDate,
        firstDate: _firstDate,
        lastDate: _lastDate,
        selectableDayPredicate: selectableDayPredicate,
        initialDatePickerMode: initialDatePickerMode,
        locale: locale,
        textDirection: textDirection,
        builder: builder,
        useRootNavigator: useRootNavigator
    );

    if(result != null){
      onDialogResult(tag, result);
    }
    onDialogClosed(tag);
  }

  ///
  /// Open bottom sheet dialog
  ///
  void openBottomSheetDialog({@required dynamic tag,
    @required BuildContext context,
    @required Widget Function(BuildContext context) builder,
    Color backgroundColor,
    double elevation,
    ShapeBorder shape,
    Clip clipBehavior,
    bool isScrollControlled = false,
    bool useRootNavigator = false,
    bool isDismissible = true}) async {

    dynamic result;

    result = await showModalBottomSheet(
      context: context,
      builder: builder,
      backgroundColor: backgroundColor,
      elevation: elevation,
      shape: shape,
      clipBehavior: clipBehavior,
      isScrollControlled: isScrollControlled,
      useRootNavigator: useRootNavigator,
      isDismissible: isDismissible,
    );

    if(result != null){
      onDialogResult(tag, result);
    }
    onDialogClosed(tag);
  }

  ///
  /// Override this if you want get the result of dialog
  ///
  /// @params tag = TAG of dialog that passed to openDialog()
  ///
  /// Note : This will only called if you open dialog by calling openDialog() before
  ///
  /// ex:
  /// class DialogWidget extends BaseState{
  ///   static const DIALOG_TAG = 0;
  ///
  ///   onDialogResult(tag, result){
  ///     if(tag == DIALOG_TAG){
  ///       do something with result
  ///     }
  ///   }
  ///
  ///   @override
  ///   Widget build(BuildContext context){
  ///     return Button(
  ///       onPressed: () => openDialog(
  ///         tag: DIALOG_TAG,
  ///         context: context,
  ///         builder: (context) => DialogContent()
  ///       )
  ///     )
  ///   }
  /// }
  ///
  /// class DialogContent extends StatelessWidget{
  ///   @override
  ///   Widget build(BuildContext context){
  ///     return Dialog(
  ///       child: Container(
  ///         child Center(
  ///           child: Button(
  ///             onPressed: () => Navigator.of(context).pop(result: true)
  ///           )
  ///         )
  ///       )
  ///     )
  ///   }
  /// }
  ///
  void onDialogResult(dynamic tag, dynamic result){

  }

  ///
  /// Override this if you want to do something when dialog closed
  ///
  /// @params tag = TAG of dialog that passed to openDialog()
  ///
  /// Note : This will only called if you open dialog by calling openDialog() before
  ///
  /// ex:
  /// class DialogWidget extends BaseState{
  ///   static const DIALOG_TAG = 0;
  ///
  ///   onDialogClosed(tag){
  ///     if(tag == DIALOG_TAG){
  ///       do something
  ///     }
  ///   }
  ///
  ///   @override
  ///   Widget build(BuildContext context){
  ///     return Button(
  ///       onPressed: () => openDialog(
  ///         tag: DIALOG_TAG,
  ///         context: context,
  ///         builder: (context) => DialogContent()
  ///       )
  ///     )
  ///   }
  /// }
  ///
  /// class DialogContent extends StatelessWidget{
  ///   @override
  ///   Widget build(BuildContext context){
  ///     return Dialog(
  ///       child: Container(
  ///         child Center(
  ///           child: Button(
  ///             onPressed: () => Navigator.of(context).pop(result: true)
  ///           )
  ///         )
  ///       )
  ///     )
  ///   }
  /// }
  ///
  void onDialogClosed(dynamic tag){

  }

  ///
  /// Called when requesting to APIs/Webservices
  /// This will open loading dialog that cannot be dismissed
  /// when you touch background or back button
  /// If you want to custom your loading dialog just override this
  ///
  /// @params typeRequest = TAG when requesting to APIs/Webservices
  ///
  /// Note : If you override this, you should override shouldHideDialog()
  ///
  void shouldShowLoading(int typeRequest){
    showDialog(context: context,
      barrierDismissible: false,
      builder: (BuildContext buildContext) => LoadingDialog(),
    );
  }

  ///
  /// Closed the loading dialog which was opened by calling
  /// shouldShowDialog
  /// If you want to custom your loading dialog just override this
  ///
  /// @params typeRequest = TAG when requesting to APIs/Webservices
  ///
  /// Note : If you override this, you should override shouldShowDialog()
  ///
  void shouldHideLoading(int typeRequest){
    Navigator.of(context).pop();
  }

  ///
  /// Called when response was failed
  /// ex: Wrong Username / Password when login, etc
  ///
  /// @params typeRequest = TAG when requesting to APIs/Webservices
  /// @params exception = the exception
  /// you can obtain message by calling msg variable of exception
  ///
  /// Override this if you want to do something when got error response
  ///
  void onResponseError(int typeRequest, ResponseException exception){
    dialog(msg: exception.msg);
  }

  ///
  /// Called when Request Time Out while requesting to APIs/Webservices
  /// RTO duration can changed in BaseRepository.dart
  /// Also will called if server throw HTTP 504
  ///
  /// @params typeRequest = TAG when requesting to APIs/Webservices
  ///
  /// Override this if you want to do something when RTO
  ///
  void onRequestTimeOut(int typeRequest){
    dialog(msg: ErrorMessaging.REQUEST_TIME_OUT_MESSAGE);
  }

  ///
  /// Called when device not connected to Internet / WIFI while requesting to APIs/Webservices
  ///
  /// @params typeRequest = TAG when requesting to APIs/Webservices
  ///
  /// Override this if you want to do something when device not connected to internet / wifi
  ///
  void onNoConnection(int typeRequest){
    dialog(msg: ErrorMessaging.NO_CONNECTION_MESSAGE);
  }

  ///
  /// Called when server throws HTTP error, ex : HTTP 404, HTTP 500, etc.
  ///
  /// @params typeRequest = TAG when requesting to APIs/Webservices
  /// @params msg = Message of HTTP error
  ///
  /// Override this if you want to do something when server throws HTTP error
  ///
  void onUnknownError(int typeRequest, String msg){
    dialog(msg: msg);
  }

  ///
  /// Open Message Dialog
  /// if Android it will open default Material Dialog that contain one action button
  /// if iOS it will open default Cupertino Dialog that contain one action button
  ///
  /// @params title = Title of dialog
  /// @params msg = Message of dialog
  /// @params actionMsg = Action Message of dialog
  /// @params action = Action when button Action clicked
  ///
  void dialog({dynamic tag = DEFAULT_DIALOG_TAG, String title = ErrorMessaging.ERROR_TITLE_MESSAGE,
    String msg = ErrorMessaging.DEFAULT_MESSAGE,
    String actionMsg = ErrorMessaging.CLOSE_ACTION_MESSAGE, Function() action}){
    if(action == null){
      action = () => Navigator.of(context).pop();
    }

    if(Platform.isIOS){
      dynamic result = showCupertinoDialog(
        context: context,
        builder: (BuildContext c) => CupertinoAlertDialog(
          title: Text(title, style: TextStyle(fontSize: 20, color: Colors.black)),
          content: Text(msg, style: TextStyle(fontSize: 16, color: Color(0x8a000000))),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(actionMsg),
              onPressed: action,
            ),
          ],
        ),
      );
      if(result != null){
        onDialogResult(tag, result);
      }
      onDialogClosed(tag);
      return;
    }

    dynamic result = showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext c) => AlertDialog(
        title: Text(title, style: TextStyle(fontSize: 20, color: Colors.black)),
        content: Text(msg, style: TextStyle(fontSize: 16, color: Color(0x8a000000))),
        actions: <Widget>[
          FlatButton(
            child: Text(actionMsg),
            onPressed: action,
          ),
        ],
      ),
    );

    if(result != null){
      onDialogResult(tag, result);
    }
    onDialogClosed(tag);
  }

  ///
  /// Open Alert Dialog
  /// if Android it will open default Material Alert Dialog that contain two action button
  /// if iOS it will open default Cupertino Alert Dialog that contain two action button
  /// else will open toast
  ///
  /// @params title = Title of dialog
  /// @params msg = Message of dialog
  /// @params negativeTitle = Negative Action Title of dialog
  /// @params positiveTitle = Positive Action Title of dialog
  /// @params onNegative = Action when Negative Button Clicked
  /// @params onPositive = Action when Positive Button Clicked
  /// @params negativeColor = Color of Title Negative Button
  /// @params positiveColor = Color of Title Positive Button
  /// @params barrierDismissible = if true dialog will closed if back button pressed
  ///
  /// Note : @params barrierDismissible always false by system in iOS
  ///
  void alert({dynamic tag = DEFAULT_ALERT_DIALOG_TAG, String title = "Error", String message = "Something went wrong", String negativeTitle,
    String positiveTitle, VoidCallback onNegative, VoidCallback onPositive,
    Color negativeColor = Colors.grey, Color positiveColor = Colors.blue, bool barrierDismissible = true}){
    if(Platform.isIOS){
      _iOSAlert(tag: tag, title: title, message: message,
        negativeTitle: negativeTitle, positiveTitle: positiveTitle,
        onNegative: onNegative, onPositive: onPositive,
        negativeColor: negativeColor, positiveColor: positiveColor,
      );
    }else {
      _androidAlert(tag: tag, title: title, message: message,
        negativeTitle: negativeTitle, positiveTitle: positiveTitle,
        onNegative: onNegative, onPositive: onPositive,
        negativeColor: negativeColor, positiveColor: positiveColor,
        barrierDismissible: barrierDismissible,
      );
    }
  }

  ///
  /// Open Android Material Alert Dialog
  /// if Android it will open default Material Alert Dialog that contain two action button
  ///
  /// @params title = Title of dialog
  /// @params msg = Message of dialog
  /// @params negativeTitle = Negative Action Title of dialog
  /// @params positiveTitle = Positive Action Title of dialog
  /// @params onNegative = Action when Negative Button Clicked
  /// @params onPositive = Action when Positive Button Clicked
  /// @params negativeColor = Color of Title Negative Button
  /// @params positiveColor = Color of Title Positive Button
  /// @params barrierDismissible = if true dialog will closed if back button pressed
  ///
  /// Note : @params barrierDismissible always false by system in iOS
  ///
  void _androidAlert({dynamic tag, String title, String message, String negativeTitle, String positiveTitle,
    VoidCallback onNegative, VoidCallback onPositive, Color negativeColor, Color positiveColor,
    bool barrierDismissible}) async {
    var result = await showDialog(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (ctx) => WillPopScope(
        child: AlertDialog(
          title: Text(title,
              style: TextStyle(fontSize: 20,
                  fontWeight: FontWeight.w500, color: Colors.black)),
          content: Text(message,
              style: TextStyle(fontSize: 16,
                  color: Colors.black)),
          actions: _alertActions(negativeTitle, positiveTitle, onNegative,
              onPositive, negativeColor, positiveColor),
        ),
        onWillPop: () async => barrierDismissible,
      ),
    );

    if(result != null){
      onDialogResult(tag, result);
    }
    onDialogClosed(tag);
  }

  ///
  /// Open iOS Cupertino Alert Dialog
  /// if Android it will open default Material Alert Dialog that contain two action button
  ///
  /// @params title = Title of dialog
  /// @params msg = Message of dialog
  /// @params negativeTitle = Negative Action Title of dialog
  /// @params positiveTitle = Positive Action Title of dialog
  /// @params onNegative = Action when Negative Button Clicked
  /// @params onPositive = Action when Positive Button Clicked
  /// @params negativeColor = Color of Title Negative Button
  /// @params positiveColor = Color of Title Positive Button
  /// @params barrierDismissible = if true dialog will closed if back button pressed
  ///
  /// Note : @params barrierDismissible always false by system in iOS
  ///
  void _iOSAlert({dynamic tag, String title, String message, String negativeTitle, String positiveTitle,
    VoidCallback onNegative, VoidCallback onPositive, Color negativeColor, Color positiveColor}) async {
    var result = await showCupertinoDialog(
        context: context,
        builder: (ctx) => CupertinoAlertDialog(
          title: Text(title,
              style: TextStyle(fontSize: 20,
                  fontWeight: FontWeight.w500, color: Colors.black)),
          content: Text(message,
              style: TextStyle(fontSize: 16,
                  color: Colors.black)),
          actions: _alertActions(negativeTitle, positiveTitle, onNegative,
              onPositive, negativeColor, positiveColor),
        )
    );

    if(result != null){
      onDialogResult(tag, result);
    }
    onDialogClosed(tag);
  }

  List<Widget> _alertActions(String negativeTitle, String positiveTitle, VoidCallback onNegative,
      VoidCallback onPositive, Color negativeColor,
      Color positiveColor){
    var actions = <Widget>[];

    if(negativeTitle != null){
      if(Platform.isIOS){
        actions.add(CupertinoDialogAction(
          child: Text(negativeTitle),
          onPressed: onNegative ?? () => finish(),
          isDefaultAction: true,
        ));
      }else {
        actions.add(FlatButton(
          child: Text(negativeTitle,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500,
                  color: negativeColor)),
          onPressed: onNegative ?? () => finish(),
        ));
      }
    }

    if(positiveTitle != null){
      if(Platform.isIOS){
        actions.add(CupertinoDialogAction(
          child: Text(positiveTitle),
          onPressed: onPositive ?? () => finish(),
          isDefaultAction: true,
        ));
      }else {
        actions.add(FlatButton(
          child: Text(positiveTitle,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500,
                  color: positiveColor)),
          onPressed: onPositive ?? () => finish(),
        ));
      }
    }

    return actions;
  }
}



